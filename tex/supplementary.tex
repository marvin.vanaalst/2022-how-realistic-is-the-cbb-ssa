% chktex-file 8
\documentclass[english]{article}
\usepackage[a4paper,total={170mm,247mm},left=20mm,top=25mm]{geometry}
\usepackage[english]{babel}
\usepackage[parfill]{parskip}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{anyfontsize}
\usepackage{glossaries}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[section]{placeins}
\usepackage{orcidlink}
\usepackage{varioref}
\usepackage{nameref}
\usepackage{cleveref}
\usepackage{xr}
\usepackage{lineno}

\newacronym{cbb}{CBB}{Calvin-Benson-Bassham}
\newacronym{petc}{pETC}{photosynthetic electron transfer chain}
\newacronym{rmse}{RMSE}{root-mean-square error}
\newacronym{mehler}{WWC}{water-water cycle}
\newacronym{pfd}{PPFD}{photosynthetically active photon flux density}
\newacronym{rubisco}{rubisco}{ribulose-1,5-bisphosphate carboxylase-oxygenase}
\newacronym{sbpase}{SBPase}{sedoheptulose-bisphosphatase}
\newacronym{ode}{ODE}{ordinary differential equation}
\newacronym{co2}{CO2}{carbon dioxide}
\newacronym{neon}{NEON}{National Ecological Observatory Network}
\newacronym{lbfgs}{L-BFGS-B}{Limited-memory Broyden-Fletcher-Goldfarb-Shanno}
\newacronym{rubp}{RuBP}{ribulose-1,5-bisphosphate}
\newacronym{e4p}{E4P}{erythrose-4-phosphate}
\newacronym{fbp}{FBP}{fructose-1,6-bisphosphat}
\newacronym{sbp}{SBP}{sedoheptulose-1-7-bisphosphate}
\newacronym{s7p}{S7P}{sedoheptulose-7-phosphate}
\newacronym{x5p}{X5P}{xylulose-5-phosphate}
\newacronym{r5p}{R5P}{ribose-5-phosphate}
\newacronym{atp}{ATP}{adenosine-triphosphate}
\newacronym{nadph}{NADPH}{nicotinamide adenine dinucleotide phosphate}
\newacronym{gsh}{GSH}{sedoheptulose-bisphosphatase}
\newacronym{gssg}{GSSG}{glutathione-disulfide}
\newacronym{tpi}{TPI}{triose-phosphate isomerase}
\newacronym{fbpase}{FBPase}{fructose-1,6-bisphophatase}
\newacronym{aldolase}{ALDOA}{fructose-bisphosphate aldolase}
\newacronym{ode-model}{M1}{ODE model}
\newacronym{ss-model}{M2}{steady-state approximated model}
\newacronym{poly-model}{M3}{polynomial model}
\newacronym{vmax}{Vmax}{maximal velocity}
\glsunset{atp}  % suppress expansion, these are clear
\glsunset{nadph} % suppress expansion, these are clear
\glsunset{ode-model}
\glsunset{ss-model}
\glsunset{poly-model}
\glsunset{co2}

% modify \thefigure, *not* \figurename
\renewcommand\thefigure{S\arabic{figure}}

\title{Supplementary information - Approximating carbon fixation - how important is the Calvin-Benson cycle steady-state assumption?}
\date{} % empty such that it isn't displayed
\author{
    Marvin van Aalst \orcidlink{0000-0002-7434-0249}
    \and
    Oliver Ebenhöh \orcidlink{0000-0002-7229-7398}
    \and
    Berkley Walker \orcidlink{0000-0001-5932-6468}
}

\begin{document}

\maketitle

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-abs-rel-tot-error-full.png}
    \caption{Absolute total error of flux predictions of the simplified model relative to the \gls{ode} model.}
    \label{fig:abs-rel-tot-error-full}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-rel-std-concs-full.png}
    \caption{Relative standard deviation (coefficient of variation) of concentrations predicted by the \gls{ode} model over a course of a 6-hour experiment.}
    \label{fig:rel-std-concs-full}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-rel-std-fluxes.png}
    \caption{Relative standard deviation (coefficient of variation) of fluxes predicted by the \gls{ode} model over a course of a 6-hour experiment.}
    \label{fig:rel-std-fluxes-full}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-rubisco-approximations.png}
    \caption{
        Steady-state scan of \gls{rubisco} flux in the \gls{ode} model over a range of \gls{pfd} values with both a Michaelis-Menten and polynomial function fitted to the obtained data.
        The Michaelis-Menten function has a \gls{rmse} of 0.22 \(\frac{mM}{s}\), while the polynomial function has a \gls{rmse} of 0.02 \(\frac{mM}{s}\).
    }
    \label{fig:rubisco-approximations}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-rubisco-approximations-prediction.png}
    \caption{Prediction of \gls*{rubisco} flux by the \gls*{ode} model, steady-state simplified model and polynomial model over a six hour time window. The legend shows the absolute total error of the flux relative to the \gls*{ode} model.}
    \label{fig:rubisco-approximations-prediction}
\end{figure*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D thing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \begin{figure}
%     \centering{}
%     \includegraphics[width=0.9\linewidth]{figures/part3-experiment-rubisco-by-rubp-and-pfd.png}
%     \caption{\ldots}
%     \label{fig:rubisco-by-rubp-and-pfd}
% \end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Collection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part0-collection-pars.png}
    \caption{Distribution of parameter values relative to the parameter values of the original model that in respective combinations lead to a carbon fixation rate within 1 \% of the original model.}
    \label{fig:emsemble-pars}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part0-collection-flux.png}
    \caption{Distribution of fluxes relative to the fluxes of the original model that in respective combinations lead to a carbon fixation rate within 1 \% of the original model.}
    \label{fig:emsemble-fluxes}
\end{figure*}


\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part0-collection-concs.png}
    \caption{Distribution of concentrations relative to the concentrations of the original model that were produced by fluxes that lead to a carbon fixation rate within 1 \% of the original model.}
    \label{fig:emsemble-concs}
\end{figure*}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data reduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part3-experiment-pfd-distribution-per-hour.png}
    \caption{\gls*{pfd} distribution per hour of day for a typical summer day.}
    \label{fig:pfd-distribution-per-hour}
\end{figure*}


\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/part2-error-by-step-size-clipped.png}
    \caption{Error of predicted carboxylation flux of the \gls*{ss-model} relative to the \gls*{ode} model depending on step size and clipping point for \gls*{pfd} values.}
    \label{fig:error-by-step-size}
\end{figure*}


\begin{figure*}
    \centering{}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering{}
        \includegraphics[width=\linewidth]{figures/part2-modified-pfd-data.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering{}
        \includegraphics[width=\linewidth]{figures/part2-modified-pfd-predictions.png}
    \end{subfigure}
    \caption{Clipping of the \gls*{pfd} input signal on left-hand side graph and prediction of \gls*{rubisco} flux for either un-clipped data (\gls*{ode-model}) or clipped data (\gls*{ss-model} and \gls*{poly-model}) on right-hand side graph.}
    \label{fig:modified-pfd-data}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part3-error-by-sample-size.png}
    \caption{Error of \gls*{ss-model} carbon fixation flux prediction relative to the \gls*{ode} model by utilizing either the mean \gls*{pfd} value of a 60 minute sample with one minute time steps, the mean flux of 60 \gls*{pfd} values or the mean flux of random samples with different sample sizes. }
    \label{fig:pfd-samples}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Low-pass-filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/bonus-low-pass-filter.png}
    \caption{Difference between the carbon fixation prediction of the \gls*{ode} for different \gls*{pfd} inputs. The base input is a sine wave centered around \gls*{pfd} 700 \(\frac{\mu mol}{s \cdot m^2} \) with an amplitude of 300 \(\frac{\mu mol}{s \cdot m^2} \) and frequency of 1 / hour, onto which a noise signal with an amplitude between 10 and 100 \(\frac{\mu mol}{s \cdot m^2} \) and a frequency between 0 and 23 / hour is added.}
    \label{fig:low-pass-filter}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MCA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-by-pfd.png}
    \caption{Concentration response coefficients to small changes in PPFD at different illumination levels.}
    \label{fig:mca-ccc-by-pfd}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-by-pfd.png}
    \caption{Flux response coefficients to small changes in PPFD at different illumination levels.}
    \label{fig:mca-fcc-by-pfd}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MCA scans
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0100.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0100}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0200.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0200}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0300.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0300}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0400.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0400}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0500.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0500}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0500.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0600}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0600.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0700}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0700.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0800}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-0900.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-0900}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1000.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1000}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1100.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1100}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1200.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1200}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1300.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1300}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1400.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1400}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-flux-cc-pfd-1500.png}
    \caption{Flux control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-flux-1500}
\end{figure*}

% Concentration

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0100.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0100}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0200.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0200}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0300.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0300}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0400.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0400}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0500.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0500}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0600.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0600}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0700.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0700}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0800.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0800}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-0900.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-0900}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1000.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1000}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1100.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1100}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1200.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1200}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1300.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1300}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1400.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1400}
\end{figure*}

\begin{figure*}
    \centering{}
    \includegraphics[width=\linewidth]{figures/mca-concentration-cc-pfd-1500.png}
    \caption{Concentration control coefficients of representative reactions of photosynthesis at specified PPFD.}
    \label{fig:mca-scan-concentration-1500}
\end{figure*}

\end{document}
