% chktex-file 8
\documentclass[twocolumn]{bioRxiv}

% Preamble
% always build supplementary.tex first for this to work
\makeatletter
\newcommand*{\addFileDependency}[1]{
\typeout{(#1)}
\@addtofilelist{#1}
\IfFileExists{#1}{}{\typeout{No file #1.}}
}\makeatother
\newcommand*{\myexternaldocument}[1]{%
\externaldocument{#1}%
\addFileDependency{#1.tex}%
\addFileDependency{#1.aux}%
}
\myexternaldocument{supplementary}
\externaldocument[supp-]{supplementary}
\addbibresource{library.bib}

\begin{document}

\title{Approximating carbon fixation - how important is the Calvin-Benson cycle steady-state assumption?}
\shorttitle{Approximating carbon fixation}
\leadauthor{van Aalst}

\author[1]{Marvin van Aalst \orcidlink{0000-0002-7434-0249}}
\author[1,2]{Oliver Ebenhöh \orcidlink{0000-0002-7229-7398}}
\author[3,4]{Berkley J. Walker \orcidlink{0000-0001-5932-6468}}

\affil[1]{
    Institute of Theoretical and Quantitative Biology,
    Heinrich Heine University Düsseldorf,
    Düsseldorf, Germany}
\affil[2] {
    Cluster of Excellence on Plant Sciences,
    Heinrich Heine University Düsseldorf,
    Düsseldorf, Germany}
\affil[3]{
    Department of Energy-Plant Research Laboratory,
    Michigan State University,
    East Lansing, MI, USA}
\affil[4]{
    Department of Plant Biology,
    Michigan State University,
    East Lansing, MI, USA}
\date{}
\maketitle

\newacronym{cbb}{CBB}{Calvin-Benson-Bassham}
\newacronym{petc}{pETC}{photosynthetic electron transfer chain}
\newacronym{rmse}{RMSE}{root-mean-square error}
\newacronym{mehler}{WWC}{water-water cycle}
\newacronym{pfd}{PPFD}{photosynthetically active photon flux density}
\newacronym{rubisco}{rubisco}{ribulose-1,5-bisphosphate carboxylase-oxygenase}
\newacronym{sbpase}{SBPase}{sedoheptulose-bisphosphatase}
\newacronym{ode}{ODE}{ordinary differential equation}
\newacronym{co2}{CO2}{carbon dioxide}
\newacronym{neon}{NEON}{National Ecological Observatory Network}
\newacronym{lbfgs}{L-BFGS-B}{Limited-memory Broyden-Fletcher-Goldfarb-Shanno}
\newacronym{rubp}{RuBP}{ribulose-1,5-bisphosphate}
\newacronym{e4p}{E4P}{erythrose-4-phosphate}
\newacronym{fbp}{FBP}{fructose-1,6-bisphosphat}
\newacronym{sbp}{SBP}{sedoheptulose-1-7-bisphosphate}
\newacronym{s7p}{S7P}{sedoheptulose-7-phosphate}
\newacronym{x5p}{X5P}{xylulose-5-phosphate}
\newacronym{r5p}{R5P}{ribose-5-phosphate}
\newacronym{atp}{ATP}{adenosine-triphosphate}
\newacronym{nadph}{NADPH}{nicotinamide adenine dinucleotide phosphate}
\newacronym{gsh}{GSH}{sedoheptulose-bisphosphatase}
\newacronym{gssg}{GSSG}{glutathione-disulfide}
\newacronym{dha}{DHA}{dehydroascorbate}
\newacronym{tpi}{TPI}{triose-phosphate isomerase}
\newacronym{fbpase}{FBPase}{fructose-1,6-bisphophatase}
\newacronym{aldolase}{ALDOA}{fructose-bisphosphate aldolase}
\newacronym{ode-model}{M1}{ODE model}
\newacronym{ss-model}{M2}{steady-state approximated model}
\newacronym{poly-model}{M3}{polynomial model}
\newacronym{vmax}{Vmax}{maximal velocity}

% suppress expansion, these are clear
\glsunset{atp}
\glsunset{nadph}
\glsunset{ode-model}
\glsunset{ss-model}
\glsunset{poly-model}
\glsunset{co2}

\begin{abstract}
Plants use light energy to produce ATP and redox equivalents for metabolism.
Since during the course of a day plants are exposed to constantly fluctuating light, the supply of ATP and redox equivalents is also fluctuating.
Further, if the metabolism cannot use all of the supplied energy, the excess absorbed energy can damage the plant in the form of reactive oxygen species.
It is thus reasonable to assume that the metabolism downstream of the energy supply is dynamic and as being capable of dampening sudden spikes in supply is advantageous, it is further reasonable to assume that the immediate downstream metabolism is flexible as well.
A flexible metabolism exposed to a fluctuating input is unlikely to be in metabolic steady-state, yet a lot of mathematical models for carbon fixation assume one for the \gls*{cbb} cycle.
Here we present an analysis of the validity of this assumption by progressively simplifying an existing model of photosynthesis and carbon fixation.
\end{abstract}

\begin{keywords}
mathematical model | photosynthesis | calvin cycle | steady-state
\end{keywords}

\begin{corrauthor}
    marvin.van.aalst\at hhu.de
\end{corrauthor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Outline
The light reactions of photosynthesis must provide the chemical energy (ATP and NADPH) needed for CO$_2$ fixation through the \gls*{cbb} cycle under rapidly fluctuating light availability.
Light fluctuates across many time scales under natural conditions, greatly complicating the challenge plants have in harvesting sufficient light energy for optimal rates of carbon fixation \cite{pearcy1990,assmann2001,slattery2018,morales2020}.
Plants possess a myriad of physiological responses to changing light intensity such as variable stomatal conductance, regulation of CBB enzymes and possibly even the activation of photorespiratory genes \cite{fu2022}.
While these factors are critical for understanding the integrated response of net assimilation to fluctuating light, we have focused this investigation on dissecting out the important assumptions for modeling the CBB and \gls*{petc} using reaction kinetic models.

There have been many excellent metabolic models representing the CBB and associated pETC activity using various frameworks that can represent steady-state or dynamic behavior \cite{arnold2011,matuszynska2019,nedbal2009,zhu2007,laisk1989}.
In this paper we use a metabolic definition of steady-state, specifically that under steady-state metabolite pool sizes are constant as well as all input and output fluxes.
We use a corresponding metabolic definition for dynamic in that it is a condition where metabolite pool sizes and internal fluxes are changing.
Note that this is a slightly different definition of a physiological dynamic model, which can represent components like the slow relaxation of non-photochemical quenching, changes in stomatal conductance and rubisco activation state \cite{taylor2017slow}.
In this work we ask how important it is to consider the dynamic response of the CBB during light fluctuations and how well the commonly applied steady-state assumption can represent metabolism.

% Topic: how to build simple, robust models
The fundamental aim of mathematical modeling is to enhance understanding of the system studied, as B.D. Hahn already noted in 1993 \cite{hahn1993}.
Thirty years later much effort is still being put into building ever more complicated models with high fidelity, which are increasingly difficult to understand.
Notably, Hahn considered models with 17-31 non-linear differential equations to be large while nowadays genome-scale modeling techniques are frequently employed, which can contain thousands of reactions \cite{milstein1979,hahn1984,hahn1987,laisk1986}.
To revisit the spirit of trying to understand the key features of a system we employ model reduction of a previously published model of the \gls*{petc} and the \gls*{cbb} cycle to elucidate the main processes controlling carbon fixation in C3 plants under dynamic conditions \cite{saadat2021}.
The resulting simplified models can serve as robust alternatives in situations where few parameters are known and the research question is only concerned with carbon fixation rate, as their predictions are in very good agreement with the predictions of the original complex model.
The three consecutive reductions we employ are first replacing the dynamic system behavior with a steady-state approximation, then replacing the steady-state system with a polynomial fitted to the predicted carbon dioxide fixation rate and lastly reducing the amount of data that is fed into the model.
We find that a good prediction of carbon fixation rate requires a remarkably low amount of model fidelity as well as data, while prediction of other fluxes requires a much better description of the underlying biochemistry.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Topic: simplification 1 - removal of dynamic metabolite change
To determine the required fidelity to represent the \gls*{cbb} cycle, \gls*{petc} and photo-protective mechanisms, we first compared simulations of a detailed \gls*{ode} model with and without a simplifying metabolic steady-state assumption \cite{saadat2021, matuszynska2019}.
For this first simplification we removed the dynamic change of metabolite concentrations by pre-calculating steady-state fluxes of an \gls*{ode} model in response to realistic field conditions using incoming \gls*{pfd} measured in one minute intervals at a National Ecological Observatory Network site in Washington state, USA \cite{neondata}.
We then compared the simulated rate of carbon fixation of the dynamic and the steady-state model for a 6 hour window from a typical summer day (shown in \cref{fig:experiment}).
There is generally good agreement between the simulation of carbon fixation (\gls*{rubisco} flux) between the two approaches.
While the relative error in short periods can exceed 50 \%, the total error of predicting the \gls*{rubisco} flux is 0.49 \%.

\begin{figure}[htbp]
    \centering{}
    \includegraphics[width=0.99\linewidth]{figures/part1-flux-over-time.png}
    \caption{Comparison of \acrshort*{ode} and steady-state approximated model predictions over a dynamic light signal (grey area).
    The left subplot shows the \acrshort*{rubisco} flux predicted by the \acrshort*{ode} and the steady-state approximated model respectively over a time course of 6 hours.
    The right subplot shows the error of the steady-state approximated model \acrshort*{rubisco} flux predictions relative to \acrshort*{ode} model predictions.
    }
    \label{fig:experiment}
\end{figure}

\begin{figure}[htb]
    \centering{}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering{}
        \includegraphics[width=\linewidth]{figures/part1-abs-rel-tot-error-selection.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \includegraphics[width=\linewidth]{figures/part1-rel-std-concs-selection.png}
    \end{subfigure}
    \caption{
    Left: absolute total error of steady-state approximated model predictions compared to \gls*{ode} model predictions.
    Right: relative standard deviation (coefficient of variation) of \gls*{ode} model predicted metabolite concentrations over a course of a 6-hour experiment.}
    \label{fig:bar-charts}
\end{figure}

% Topic: investigation of why steady-state approximation works
To understand why the steady-state model behaves similarly to the dynamic model with regard to total carbon fixation despite the lack of dynamic interaction we investigated how well other fluxes are predicted and how much the concentrations change over the course of the experiment.
Shown in \cref{fig:bar-charts} is the absolute total difference between the simulations of key \gls*{cbb}, \gls*{petc} and acclimation fluxes and the relative standard deviation (coefficient of variation) of representative concentrations in the dynamic model.
The remaining fluxes and concentrations are displayed in supplementary \cref{supp-fig:abs-rel-tot-error-full,supp-fig:rel-std-concs-full} respectively and the relative standard deviation for the fluxes in supplementary \cref{supp-fig:rel-std-fluxes-full}.
In contrast to the the small difference between the \gls*{rubisco} flux in the simulations, the difference between rapid acclimation response mechanisms such as the xanthophyll cycle (zeaxanthin epoxidase) or reactions of the water-water cycle (glutathione reductase) is between 20 \% and 50~\%.
Similarly, the relative standard deviation of the \gls*{rubisco} substrate \gls*{rubp} is comparatively small, varying 15 \% of its mean value compared to the large relative standard deviation of some metabolites that take part in the rapid acclimation response, e.g.~\gls*{gssg} and \gls*{dha} which vary nearly up to 350 \% of their mean value.
Notably, other \gls*{cbb} cycle intermediates like \gls*{sbp} and \gls*{s7p} vary up to 50 \% while the difference between the flux predictions of \gls*{sbpase} is comparable to the one of \gls*{rubisco}.

\begin{figure}[htb]
    \centering{}
    \includegraphics[width=0.99\linewidth]{figures/part0-collection-flux-selection.png}
    \caption{Box-plot of \gls*{cbb} cycle fluxes in model collection with randomly perturbed kinetic parameters relative to the fluxes in model with reference parameters.}
    \label{fig:collection}
\end{figure}


% Topic: many kinetics can lead to the same carbon fixation
To understand how alternative parameterizations of the model that lead to similar carbon fixation rates would effect internal fluxes of the CBB cycle we generated 100,000 sets of randomly perturbed kinetic parameters.
For these parameter-sets we calculated the steady state flux and then selected the ones for which the carbon fixation flux was within 1 \% of the original model, which was the case for 321 of them.
\cref{fig:collection} shows a box-plot of simulated \gls*{cbb} cycle fluxes of the selected parameter sets relative to the dynamic model, the remaining fluxes are shown in supplementary \cref{supp-fig:emsemble-fluxes}.
Some reactions of the \gls*{cbb} cycle must vary less than 10 \% to ensure a similar flux, while e.g. \gls*{tpi}, \gls*{fbpase} and \gls*{aldolase} can vary more than 30 \%.
Reactions related to triose phosphate export %(vpga, vgap, vdhap)
and storage %(vstarch, vpgm, vg6p_isomerase)
can vary upwards of 100 \%.

% Topic: simplification 2 - removal of entire CBB cycle
Given the stability of the \gls*{rubp} concentration during our simulations across irradiances and the insensitivity of rubisco flux to parameterization of many CBB model reactions, we hypothesized that it is possible to vastly simplify the \gls*{cbb} cycle and still get a reasonable prediction.
To describe the carbon fixation rate as being only dependent on irradiance, we replaced the explicit \gls*{cbb} model with a 4th degree polynomial function which we fitted over simulated carbon fixation fluxes.
The agreement between these two models was very good, with a very small \gls*{rmse} of 0.02 \(\frac{mM}{s}\) (shown in supplementary \cref{supp-fig:rubisco-approximations}).
When simulated over the same data shown in \cref{fig:experiment}, the difference between the total \gls*{rubisco} flux of the polynomial model and the dynamic model \gls*{ode} fluxes is 0.64 \% and thus comparable to the difference of 0.49 \% between the steady-state model and the dynamic model (shown in supplementary \cref{supp-fig:rubisco-approximations-prediction}).

% Topic: simplification 3 -  mean over an hour instead of high-resolution data
So far we have used high temporal resolution data in our simulations, but data availability, lack of computational power or technical difficulties due to multi-scale modeling can require the use of low temporal resolution data.
A common practice to reduce the temporal resolution is to simply average the \gls*{pfd} values over longer time periods.
In our experiment, an increase from one minute to 60 minute steps increased the difference of carbon fixation from 0.49 \% to roughly 7 \% (see supplementary \cref{supp-fig:error-by-step-size}).

% Topic: Unexpectedly high error due to average
While an increase in error with fewer data was to be expected, we hypothesized that the simulation can be improved substantially by using an alternative averaging approach.
If carbon assimilation responded linearly to irradiance, carbon assimilation over the average irradiance of a time period would equal the average of carbon assimilation over that time period.
In other words, irradiance could be averaged over a time period with fluctuating light and simulate an equal total amount of carbon fixation as the fluctuating light period.
However, as carbon fixation responds non-linearly to irradiance, the simulated carbon fixation of the average irradiance does not lead to the average of the simulated carbon fixation over fluctuating light.
In the case of the saturating response of carbon fixation to irradiance, simple averaging would lead to over-estimates over periods of fluctuating irradiance.
% To counteract this bias, an exact function can in principle be determined, however that function might be hard to obtain.

\begin{figure}[tb]
    \centering{}
    \includegraphics[width=0.99\linewidth]{figures/part2-absolute-total-error-per-day-selection.png}
    \caption{Total prediction error of carbon fixation of the steady-state approximated model (60 minute resolution) and the polynomial model (60 minute resolution) relative to the \gls*{ode} model (1 minute resolution). Results are shown for both the mean \gls*{pfd} value of the raw data and the mean value of the data clipped at either \gls*{pfd} 900 (steady-state approximated model) or 1000 (polynomial model).}
    \label{fig:error-per-month}
\end{figure}

% Topic: Clipping to counteract bias
To produce representative \gls*{pfd} values that avoid the bias of simple averaging over each time period, we clipped the input data by capping saturating \gls*{pfd} values to reduce their effect before calculating the mean.
As this approach led to promising results with our single-day data we expanded the analysis to a representative day for each month of the entire year, and then simulated the carbon fixation rate of this day for each month with one minute steps for the \gls*{ode} model and 60 minute steps for the polynomial model.

For the polynomial model we included both the prediction with the mean of the raw data and the mean of the clipped data.
\cref{fig:error-per-month} shows the total error of the \gls*{rubisco} flux prediction of the polynomial model relative to the \gls*{ode} model per month.
As indicated in the figure legend, the total error per year of the polynomial model relatives to the \gls*{ode} model is 4.0~\% for raw data and 0.1 \% for data clipped at a \gls*{pfd} of 1000.
The absolute of the error of the polynomial model for October to February is between 5 and 15 \%, while it is lower for the summer months (except June), however if the data is clipped at a \gls*{pfd} of 1000 there is an improved fit relative to the polynomial model for the winter months.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Discussion \& Conclusions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Topic: Steady-state nature of the CBB cycle under fluctuating light
Our simulations reveal that accounting for the dynamic change of metabolite concentrations under fluctuating light results in only small differences in the predicted carbon fixation rate relative to assuming a metabolic steady state in a combined model including the CBB and light reactions.
Specifically, in \cref{fig:experiment} we compared predictions of an \gls*{ode} model with a simplified version in which we removed the dynamic change of metabolite concentrations and instead pre-calculated the steady-state fluxes for a range of \gls*{pfd} inputs.
Despite the rapid light fluctuations in the input data, our results show that the predicted total carbon fixation essentially stays the same, even though the difference of the prediction of the dynamic state can exceed 50 \% for some time steps.
This means that for the purpose of predicting total carbon fixation, our \gls*{ode} model of carbon fixation can be greatly simplified using steady-state assumptions with little difference - saving time and energy by avoiding computationally costly numerical integration.
We expect this to hold for other \gls*{ode} models of biological systems that are structured similarly.
Next we investigated how such a simplified model, which does a mechanistically poor job at representing the underlying biochemistry, does a good job at representing carbon fixation.
For this we turned to other biochemical predictions of the model.

% Topic: Bad prediction of rapid acclimation mechanisms
While total carbon fixation was predicted well in the steady-state model, the fluxes of rapid acclimation mechanisms (e.g. violaxanthin deepoxidase) were predicted poorly, with relative errors exceeding 50 \% (see \cref{fig:bar-charts}).
These observations can be explained by the amount of dynamic change in concentration over time of specific metabolite pools that directly influence carbon fixation.
For example, the \gls*{rubp} pool is comparatively stable (varying 15 \% of its mean concentration), while the \gls*{gssg} pool can vary over 350 \% of its mean concentration (see \cref{fig:bar-charts}).
Since the \gls*{rubp} pool comprises the only dynamic substrate for carbon fixation in the original model it has the largest effect on rates of rubisco carboxylation, reflected by the small relative error in this reaction of 0.49 \%.
The higher errors of acclimation and \gls*{petc} intermediates in contrast contribute very little to the error of rubisco carboxylation, as they mostly depend on prior perturbations of the system but always tend towards stabilizing the system towards the respective steady state.
Notably, other \gls*{cbb} cycle intermediates like \gls*{sbp} can also show a variation of more than 50 \% of their mean concentration, but these errors are not reflected in the downstream concentrations of \gls*{rubp}, resulting in less differences in rates of rubisco carboxylation.

% Topic: some fluxes can vary a lot in collection
One explanation for the different variation over time in metabolite pool sizes is that multiple flux distributions can lead to the same carbon fixation rate, thus allowing a wider range of concentrations for certain \gls*{cbb} cycle intermediates.
This buffering effect can be seen in \cref{fig:collection}, where in models with perturbed kinetic parameters but similar rates of carbon fixation some reactions like FBPase can vary more than a third of their reference flux.
This suggests that the \gls*{cbb} cycle is structured such that temporary changes in illumination are buffered by the cycle for \gls*{rubp} concentration to remain relatively constant, resulting in similar rates of carbon fixation, effectively working as a low-pass filter (see supplementary \cref{supp-fig:low-pass-filter,supp-fig:emsemble-concs} and supplementary \cref{supp-fig:mca-scan-flux-0100} to \cref{supp-fig:mca-scan-concentration-1500}).
Due to this stability, carbon fixation can be accurately simulated, even if the underlying metabolism is not.
While the stability of \gls*{rubp} is advantageous for downstream metabolism this also implies that alternative energy sinks like quenching mechanisms and the \gls*{mehler} are required to dissipate further excess energy.
The reactions with the most variation relate to sucrose and starch partitioning (e.g.\ FBPase, FBP aldolase and TPI), suggesting that while carbon fixation is simulated accurately, downstream carbon partitioning may not be.
These findings suggest that carbon fixation can be simulated accurately in an even more simplified \gls*{cbb} cycle, providing downstream carbon partitioning is not of interest.
Our findings that an explicit model of the \gls*{cbb} cycle can be replaced with a simple polynomial model with little sacrifice, see Figure~\ref{fig:error-per-month}, are in line with many crop-systems models that represent net carbon assimilation using a simple radiation use efficiency (e.g. % BW: Citation

% Topic: concrete suggestions that follow from the above
The discussion above highlights that metabolic models of the \gls*{cbb} cycle can produce similar rates of carbon fixation despite mechanistically curedely simplified assumptions of the underlying biochemistry. This finding indicates that care is needed when validating a model using only carbon fixation rates, since any model that keeps \gls*{rubp} concentration stable can lead to realistic carbon fixation rates.
Possible improvements include using predictions of other metabolite pools (\cite{arnold2011}) or fluorescence data if the model also contains the \gls*{petc} (\cite{saadat2021}).
Further work can test the ability to make steady-state assumptions with more complex models of carbon assimilation that include for example photorespiration, \gls*{co2} as a dynamic variable or dynamic temperature.
Photorespiration may present an interesting case since large pools of glycine accumulate during photorespiratory induction, effectively decreasing relative rates of glycine decarboxylation (and increasing net carbon exchange) during this transient by up to 40 \% \cite{berkleyfixesthis}. % BW: Fix

% Topic: mean transformations for saturating kinetics
We show that temporally high-resolution \gls*{pfd} data is not required to give a good prediction of total carbon fixation as reducing the amount of data by 60-fold still led to an prediction error of less than 1 \% if over modified averaging approach is used, see \cref{fig:error-per-month}.
The ability to properly aggregate data is important, since often all data needed to produce and validate a model are not available on the same time resolutions.
For example, in canopy scale predictions, irradiance values are available at time scales of seconds, while eddy covariance data is usually presented over 30 minute time steps.
Our modeling indicates that clipping saturating values before averaging improved prediction error by \( \approx \) 14-fold, see \cref{fig:error-per-month}.
While these findings demonstrate the value of clipping saturating values before averaging, considering the saturating kinetics of carbon fixation to \gls*{pfd}, this method could be valuable for any high-resolution data which needs to be averaged over a time-step in a process with saturating kinetics.


\section*{Methods}

We performed our analyses in Python 3.10, utilising the common packages NumPy, pandas, SciPy and Matplotlib for general data analysis as well as modelbase and Assimulo for building and integrating the \gls*{ode} model \cite{python310,numpy, pandas, scipy, matplotlib, modelbase, assimulo}.
All code used to generate the publication results and figures is publicly available on our GitLab repository \url{https://gitlab.com/qtb-hhu/photosynthesis-task-force/2022-how-realistic-is-the-cbb-ssa}.
We obtained field observation \gls*{pfd} data measured in one minute intervals in Washington state (latitude 45.790835, longitude -121.933788) by the \gls*{neon} for which we identified which day had the highest data coverage for each month, which was the 25th, and the \gls*{ode} model from Saadat 2021 \cite{neondata, saadat2021}.
Due to model instabilities for \gls*{pfd} values below 30, we clamped the minimum of the data to 30.

\subsection*{Steady-state model}
We calculated the steady state fluxes of the \gls*{ode} model for \gls*{pfd} values between the minimal and maximal values found in the dataset with step size 1.
Then we simulated the model by looking up the steady state flux of the \gls*{pfd} value rounded to the nearest integer.

\subsection*{Approximations}

We fitted the \(V_{\max}\) and \(K_m\) parameters of the Michaelis-Menten function using the SciPy \texttt{minimize} function and the L-BFGS-B algorithm \cite{scipy, lbfgsb}.
The polynomial fit was performed using NumPy's \texttt{polyfit} function \cite{numpy}.

\section*{Funding}
This work was funded by the Deutsche Forschungsgemeinschaft (DFG) under Germany's Excellence Strategy EXC 2048/1, Project ID: 390686111 (O.E.), EU's Horizon 2020 research and innovation programme under the Grant Agreement 862087 (M.v.A.) and also supported by the U.S. Department of Energy Office of Science, Basic Energy Sciences under Award DE- FG02-91ER20021 (B.J.W.).

\section*{References}

\printbibliography


\end{document}
