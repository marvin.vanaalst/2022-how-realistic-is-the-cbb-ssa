{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eb8ae46b",
   "metadata": {},
   "source": [
    "# Applicability of steady-state assumptions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e2bb80c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "import math\n",
    "from functools import partial\n",
    "from pathlib import Path\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "from constants import calvin_cycle_intermediates, colors, long_rxn_names\n",
    "from matplotlib import pyplot as plt\n",
    "from modelbase.ode import mca\n",
    "from tqdm.contrib.concurrent import process_map\n",
    "from utils import *\n",
    "\n",
    "\n",
    "IMG_PATH = Path(\"..\") / \"tex\" / \"figures\"\n",
    "\n",
    "sns.set_theme(context=\"talk\", style=\"whitegrid\")\n",
    "\n",
    "font_scale = 1.5\n",
    "\n",
    "SM = 10\n",
    "MD = 16\n",
    "LG = 18\n",
    "\n",
    "plt.rc(\"font\", size=SM * font_scale)\n",
    "plt.rc(\"legend\", fontsize=SM * font_scale)\n",
    "plt.rc(\"xtick\", labelsize=MD * font_scale)\n",
    "plt.rc(\"ytick\", labelsize=MD * font_scale)\n",
    "plt.rc(\"axes\", labelsize=MD * font_scale)\n",
    "plt.rc(\"axes\", titlesize=LG * font_scale)\n",
    "plt.rc(\"figure\", titlesize=LG * font_scale)\n",
    "\n",
    "\n",
    "def format_xticklabels(\n",
    "    ax: Axis, rotation: float | None = None, ha: str | None = None\n",
    ") -> None:\n",
    "    ax.set_xticks(ax.get_xticks(), ax.get_xticklabels(), rotation=rotation, ha=ha)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0d71212",
   "metadata": {},
   "source": [
    "## Data\n",
    "\n",
    "The data are from field observations from MART [Washington state, geographic coordinates (lat/long datum): 45.790835 -121.933788 WGS 84], where the photosynthetically active radiation ($\\frac{\\mu mol}{s \\cdot m^2}$) was measured in one minute steps. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9dece0e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Path(\".\") / \"data\" / \"1min\"\n",
    "data_by_one_minute = pd.concat([pd.read_csv(i) for i in sorted(data.glob(\"*\"))])  # type: ignore\n",
    "data_by_one_minute.index = pd.to_datetime(data_by_one_minute[\"startDateTime\"].values)\n",
    "data_by_one_minute = data_by_one_minute[\"PARMean\"].dropna()\n",
    "data_by_one_minute.index = data_by_one_minute.index.tz_convert(\"EST\")  # type: ignore\n",
    "\n",
    "# There is some negative data, and for which pfd < 30 model is unstable\n",
    "data_by_one_minute[data_by_one_minute < 30] = 30"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4164b38f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose random date as initial experiment\n",
    "date = \"2018-06-03\"\n",
    "day: pd.Series = data_by_one_minute.loc[f\"{date} 00:00\":f\"{date} 23:59\"]  # type: ignore\n",
    "experiment: pd.Series = day.between_time(\"12:00\", \"17:59\").round(decimals=0)  # type: ignore\n",
    "\n",
    "fig = plot_pfd_of_experiment(experiment, day, date, figsize=(16, 9))\n",
    "plt.savefig(IMG_PATH / \"part1-ppfd.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb1602a4",
   "metadata": {},
   "source": [
    "## Part 1: reducing model complexity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b011ecb6",
   "metadata": {},
   "source": [
    "### Steady-state fluxes explain most of the variation\n",
    "\n",
    "RuBisCO fluxes vary quite a lot over time, but most of that behaviour is explained by the flux the system would have at steady state with each PFD. There are some large errors, but most seem to wobble around 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9aaa2ce",
   "metadata": {},
   "outputs": [],
   "source": [
    "y_ss, c_ode, v_ode, ss_concentrations_by_pfd, ss_fluxes_by_pfd = run_experiment(\n",
    "    experiment\n",
    ")\n",
    "v_dss_1 = simulate_dss_with_par(experiment, ss_fluxes_by_pfd, 1)\n",
    "\n",
    "fig, axs = plot_dss_error_over_time(experiment, v_ode, v_dss_1, figsize=(18, 8))\n",
    "\n",
    "plt.savefig(IMG_PATH / \"part1-flux-over-time.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4ae12da0",
   "metadata": {},
   "outputs": [],
   "source": [
    "cci = ss_concentrations_by_pfd[calvin_cycle_intermediates]\n",
    "cv = cci.std() * 100 / cci.mean()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(20, 8))\n",
    "cv.plot(\n",
    "    kind=\"bar\",\n",
    "    ax=ax,\n",
    "    ylabel=r\"$\\frac{\\sigma}{\\mu}$ / %\",\n",
    "    title=\"CV of CBB intermediates over PFD\",\n",
    ")\n",
    "ax.grid(False, axis=\"x\")\n",
    "format_xticklabels(ax, rotation=45, ha=\"right\")\n",
    "plt.savefig(IMG_PATH / \"part0-cv-concs-over-pfd.png\", bbox_inches=\"tight\", dpi=200)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66a226fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(20, 8))\n",
    "sns.boxenplot(data=cci, ax=ax)\n",
    "ax.set(\n",
    "    title=\"Distribution of SS concentrations in PPFD scan\", ylabel=\"Concentration / mM\"\n",
    ")\n",
    "format_xticklabels(ax, rotation=45, ha=\"right\")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part0-boxenplot-concs-over-pfd.png\", bbox_inches=\"tight\", dpi=200\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a3d2c80",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_range = cci.max() - cci.min()\n",
    "# data_range / cci.mean()\n",
    "fig, ax = plt.subplots(figsize=(20, 8))\n",
    "data_range.plot(\n",
    "    kind=\"bar\", ylabel=\"(max - min) / mM\", title=\"Range of concentrations\", ax=ax\n",
    ")\n",
    "format_xticklabels(ax, rotation=45, ha=\"right\")\n",
    "plt.savefig(IMG_PATH / \"part0-range-concs-over-pfd.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7bdbe38",
   "metadata": {},
   "source": [
    "### Only NPQ / Mehler are badly predicted\n",
    "\n",
    "What is the absolute error of total carbon fixation rate of the simplified model? 0.49 %.\n",
    "That's not a lot at all. The only fluxes that are badly predicted are the ones of non-photochemical quenching or the water-water cycle. So our model essentially predicts that the CBB cycle is close to steady state the entire time and that the quenching mechanisms buffer away most disturbances of the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a19a4a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, _ = plot_absolute_total_error_by_metabolite(\n",
    "    v_ode,\n",
    "    v_dss_1,\n",
    "    colors=colors,\n",
    "    selection=[\n",
    "        \"vPS2\",\n",
    "        \"vPS1\",\n",
    "        \"vRuBisCO\",\n",
    "        \"v9\",\n",
    "        \"vSt12\",\n",
    "        \"vLhcprotonation\",\n",
    "        \"vEpox\",\n",
    "        \"vMehler\",\n",
    "        \"vGR\",\n",
    "    ],\n",
    "    figsize=(8, 8),\n",
    "    long_rxn_names=long_rxn_names,\n",
    ")\n",
    "\n",
    "format_xticklabels(fig.gca(), rotation=45, ha=\"right\")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part1-abs-rel-tot-error-selection.png\", bbox_inches=\"tight\", dpi=200\n",
    ")\n",
    "\n",
    "fig, _ = plot_absolute_total_error_by_metabolite(\n",
    "    v_ode,\n",
    "    v_dss_1,\n",
    "    colors=colors,\n",
    "    figsize=(18, 12),\n",
    "    long_rxn_names=long_rxn_names,\n",
    ")\n",
    "plt.savefig(IMG_PATH / \"part1-abs-rel-tot-error-full.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fd217a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "selection = [\n",
    "    \"ATP\",\n",
    "    \"NADPH\",\n",
    "    \"PQ\",\n",
    "    \"RUBP\",\n",
    "    \"S7P\",\n",
    "    \"FBP\",\n",
    "    \"E4P\",\n",
    "    \"SBP\",\n",
    "    \"Fd\",\n",
    "    \"Vx\",\n",
    "    \"MDA\",\n",
    "    \"GSSG\",\n",
    "]\n",
    "\n",
    "fig = plot_relative_std(c_ode=c_ode, selection=selection, figsize=(8, 8), colors=colors)\n",
    "format_xticklabels(fig.gca(), 45, \"right\")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part1-rel-std-concs-selection.png\", bbox_inches=\"tight\", dpi=200\n",
    ")\n",
    "\n",
    "\n",
    "fig = plot_relative_std(c_ode=c_ode, colors=colors, figsize=(16, 8))\n",
    "format_xticklabels(fig.gca(), 45, \"right\")\n",
    "plt.savefig(IMG_PATH / \"part1-rel-std-concs-full.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c952facb",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_relative_std_fluxes(\n",
    "    v_ode, colors=colors, long_rxn_names=long_rxn_names, figsize=(18, 8)\n",
    ")\n",
    "format_xticklabels(fig.gca(), 45, \"right\")\n",
    "plt.savefig(IMG_PATH / \"part1-rel-std-fluxes.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "868ab9e2",
   "metadata": {},
   "source": [
    "### Carbon fixation rate is MM-like\n",
    "\n",
    "A scan over the steady-state fluxes of carbon fixation rate in our model shows that the carbon fixation rate depends of the PFD in a michaelis-menten rate equation like fashion, which suggests that the remaining metabolism will behave linearly with that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a1e52fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plot_rubisco_flux_by_rubp(experiment, y_ss=y_ss)\n",
    "plt.savefig(IMG_PATH / \"part1-rubisco-by-rubp.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "840b8451",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_ss_rubisco_flux_per_pfd(\n",
    "    v_ode,\n",
    "    ss_fluxes_by_pfd,\n",
    "    experiment,\n",
    "    min_line=False,\n",
    "    max_line=False,\n",
    "    markers=False,\n",
    "    figsize=(8, 8),\n",
    ")\n",
    "plt.savefig(IMG_PATH / \"part1-rubisco-by-pfd.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "819dc9fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plot_rubisco_flux_by_rubp_and_pfd(experiment, ss_concentrations_by_pfd)\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part1-rubisco-by-rubp-and-pfd.png\", bbox_inches=\"tight\", dpi=200\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a28a2d5a",
   "metadata": {},
   "outputs": [],
   "source": [
    "mmfit, polyfit = fit_mm_and_poly(ss_fluxes_by_pfd, 5)\n",
    "fig = plot_fit_mm_and_poly(ss_fluxes_by_pfd, mmfit, polyfit)\n",
    "plt.savefig(IMG_PATH / \"part1-rubisco-approximations.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b04a0099",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plot_rubisco_approximations_prediction(\n",
    "    experiment, v_ode, ss_fluxes_by_pfd, polyfit=polyfit, mmfit=mmfit, figsize=(16, 7)\n",
    ")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part1-rubisco-approximations-prediction.png\",\n",
    "    bbox_inches=\"tight\",\n",
    "    dpi=200,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f18a3f2",
   "metadata": {},
   "source": [
    "## Part 2: time steps\n",
    "\n",
    "So far the model is still very data-hungry. Can we improve on that?\n",
    "Let's start by using the mean of the data in given blocks of time.\n",
    "For steps of 30 minutes we are already at roughly 4 % error and for 60 minute steps we are at 7 %. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa6ab6ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_ss_rubisco_flux_per_pfd(\n",
    "    v_ode,\n",
    "    ss_fluxes_by_pfd,\n",
    "    experiment,\n",
    "    min_line=False,\n",
    "    max_line=False,\n",
    "    markers=True,\n",
    "    figsize=(8, 8),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1070d348",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(\n",
    "    {\n",
    "        1: v_dss_1[\"vRuBisCO\"],\n",
    "        2: simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd, 2),\n",
    "        5: simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd, 5),\n",
    "        10: simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd, 10),\n",
    "        20: simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd, 20),\n",
    "        60: simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd, 60),\n",
    "    }\n",
    ")  # type: ignore\n",
    "\n",
    "fig = plot_rubisco_error_per_step_size(v_ode, df, figsize=(8, 8))\n",
    "\n",
    "# plt.savefig(IMG_PATH / \"part2-error-by-step-size.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2053d13",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_rubisco_error_per_step_size(v_ode, df, figsize=(16, 8), label=\"Raw\")\n",
    "ax = fig.axes[0]\n",
    "\n",
    "for i in [\n",
    "    1500,\n",
    "    1300,\n",
    "    1100,\n",
    "    900,\n",
    "]:\n",
    "    df_clipped = pd.DataFrame(\n",
    "        {\n",
    "            1: simulate_rubisco_with_dss(experiment.clip(upper=i), ss_fluxes_by_pfd, 1),\n",
    "            2: simulate_rubisco_with_dss(experiment.clip(upper=i), ss_fluxes_by_pfd, 2),\n",
    "            5: simulate_rubisco_with_dss(experiment.clip(upper=i), ss_fluxes_by_pfd, 5),\n",
    "            10: simulate_rubisco_with_dss(\n",
    "                experiment.clip(upper=i), ss_fluxes_by_pfd, 10\n",
    "            ),\n",
    "            20: simulate_rubisco_with_dss(\n",
    "                experiment.clip(upper=i), ss_fluxes_by_pfd, 20\n",
    "            ),\n",
    "            60: simulate_rubisco_with_dss(\n",
    "                experiment.clip(upper=i), ss_fluxes_by_pfd, 60\n",
    "            ),\n",
    "        }\n",
    "    )  # type: ignore\n",
    "    plot_rubisco_error_per_step_size(v_ode, df_clipped, label=f\"Clip(upper={i})\", ax=ax)\n",
    "ax.legend()\n",
    "\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part2-error-by-step-size-clipped.png\", bbox_inches=\"tight\", dpi=200\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb6ef872",
   "metadata": {},
   "outputs": [],
   "source": [
    "# PFD distribution per hour\n",
    "\n",
    "fig = plot_pfd_distribution_per_hour(experiment)\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part2-pfd-distribution-per-hour.png\", bbox_inches=\"tight\", dpi=200\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "558b38dd",
   "metadata": {},
   "source": [
    "## Correct the weighted mean"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de52d5a4",
   "metadata": {},
   "outputs": [],
   "source": [
    "pars_poly = [5.0e-11, 1.0e-07, 1.0e-08, 1.0e-04, 485]\n",
    "\n",
    "\n",
    "fig = plot_modified_pfd_data(experiment, figsize=(8, 8))\n",
    "ax = fig.gca()\n",
    "format_xticklabels(ax, rotation=45, ha=\"right\")\n",
    "plt.savefig(IMG_PATH / \"part2-modified-pfd-data.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b3425e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_predictions_with_corrected_data(\n",
    "    experiment: pd.Series,\n",
    "    v_ode: pd.DataFrame,\n",
    "    ss_fluxes_by_pfd: pd.DataFrame,\n",
    "    polyfit: Any,\n",
    "    figsize: tuple[float, float],\n",
    ") -> Figure:\n",
    "\n",
    "    y_true = v_ode[\"vRuBisCO\"].sum()\n",
    "\n",
    "    y_pred_raw = simulate_dss_with_par(experiment, ss_fluxes_by_pfd, 60)[\"vRuBisCO\"]\n",
    "    y_pred_clip = simulate_dss_with_par(\n",
    "        experiment.clip(upper=900), ss_fluxes_by_pfd, 60\n",
    "    )[\"vRuBisCO\"]\n",
    "    y_pred_clip_poly = simulate_rubisco_with_polyfit(\n",
    "        experiment.clip(upper=900), polyfit, 60\n",
    "    )\n",
    "\n",
    "    x = experiment.index[0] + pd.to_timedelta(v_ode.index, unit=\"s\")  # type: ignore  # type: ignore\n",
    "\n",
    "    fig, ax = plt.subplots(figsize=figsize)\n",
    "    ax.plot(x, v_ode[\"vRuBisCO\"], label=\"ODE\")\n",
    "    ax.plot(\n",
    "        x,\n",
    "        y_pred_raw,\n",
    "        label=f\"DSS(PFD), error = {abs(y_true - y_pred_raw.sum()) / y_true:.1%}\",\n",
    "    )\n",
    "    ax.plot(\n",
    "        x,\n",
    "        y_pred_clip,\n",
    "        label=f\"DSS(clip(PFD)), error = {abs(y_true - y_pred_clip.sum()) / y_true:.1%}\",\n",
    "    )\n",
    "    ax.plot(\n",
    "        x,\n",
    "        y_pred_clip_poly,\n",
    "        label=f\"polyfit(clip(PFD)), error = {abs(y_true - y_pred_clip_poly.sum()) / y_true:.1%}\",\n",
    "    )\n",
    "\n",
    "    ax.legend()\n",
    "    # Shade experiment\n",
    "    ax2 = ax.twinx()\n",
    "    ax2.fill_between(experiment.index, experiment.values, color=\"black\", alpha=1 / 16)  # type: ignore\n",
    "    ax.set_title(\"Carbon fixation predictions\")\n",
    "    return fig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5325d633",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_predictions_with_corrected_data(\n",
    "    experiment, v_ode, ss_fluxes_by_pfd, polyfit=polyfit, figsize=(8, 8)\n",
    ")\n",
    "fig.autofmt_xdate()\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part2-modified-pfd-predictions.png\", bbox_inches=\"tight\", dpi=200\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1cc1af3e",
   "metadata": {},
   "source": [
    "## Check for all months\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03c41bb7",
   "metadata": {},
   "outputs": [],
   "source": [
    "coverage_per_day = get_coverage_per_day(data_by_one_minute[\"2018\":\"2018\"])  # type: ignore\n",
    "day_with_most_coverage = get_day_with_most_coverage(coverage_per_day)\n",
    "valid_months = coverage_per_day.loc[day_with_most_coverage].dropna().index  # type: ignore\n",
    "print(day_with_most_coverage)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e37b640",
   "metadata": {},
   "outputs": [],
   "source": [
    "worker = partial(\n",
    "    simulate_day_by_hours,\n",
    "    model_fn=get_model,\n",
    "    ss_concentrations_by_pfd=ss_concentrations_by_pfd,\n",
    "    day_with_most_coverage=day_with_most_coverage,\n",
    "    data_by_one_minute=data_by_one_minute,  # type: ignore\n",
    ")\n",
    "# This takes roughly 5 minutes\n",
    "experiments_split: dict[int, dict[int, Experiment]] = dict(\n",
    "    process_map(worker, valid_months)\n",
    ")\n",
    "\n",
    "predictions = get_predictions_per_month(experiments_split, ss_fluxes_by_pfd, polyfit)\n",
    "\n",
    "fig = plot_errors_per_month(\n",
    "    predictions,\n",
    "    # selection=[\"dss\", \"polyfit\", \"dss clipped @ 900\", \"polyfit clipped @ 1000\"],\n",
    "    selection=[\"polyfit\", \"polyfit clipped @ 1000\"],\n",
    "    plot_names={\n",
    "        \"polyfit\": \"Polynomial\",\n",
    "        \"polyfit clipped @ 1000\": \"Polynomial (clipped)\",\n",
    "    },\n",
    ")\n",
    "format_xticklabels(fig.gca(), 45, \"right\")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part2-absolute-total-error-per-day-selection.png\",\n",
    "    bbox_inches=\"tight\",\n",
    "    dpi=200,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46b62c4c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot_errors_per_month(\n",
    "    predictions,\n",
    "    legend_kwargs=dict(\n",
    "        loc=\"upper left\",\n",
    "        bbox_to_anchor=(1.01, 1),\n",
    "        borderaxespad=0,\n",
    "    ),\n",
    ")\n",
    "plt.savefig(\n",
    "    IMG_PATH / \"part2-absolute-total-error-per-day-all.png\",\n",
    "    bbox_inches=\"tight\",\n",
    "    dpi=200,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d11a0763",
   "metadata": {},
   "source": [
    "## Bonus: Distribution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77673dd4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using distribution instead of mean value\n",
    "\n",
    "short_experiment = day.between_time(\"12:00\", \"12:59\").round(decimals=0)\n",
    "\n",
    "s = Simulator(get_model(short_experiment.iloc[0]))\n",
    "s.initialise(y_ss)\n",
    "result = simulate_ode_with_par(s, short_experiment)\n",
    "v_ode = cast(SimulationResult, result).fluxes\n",
    "fig = plot_pfd_distribution_and_mean(short_experiment)\n",
    "fig = plot_error_distribution_by_sample_size(short_experiment, v_ode, ss_fluxes_by_pfd)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a956d40c",
   "metadata": {},
   "source": [
    "## Bonus: CBB as low-pass filter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acce4cae",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_end = 60  # one hour\n",
    "x = np.linspace(0, np.pi, t_end)\n",
    "time = np.linspace(0, t_end, len(x))\n",
    "\n",
    "main_frequency = 1\n",
    "main_amplitude = 300\n",
    "main_signal = (700 - main_amplitude) + (\n",
    "    np.sin(2 * main_frequency * x) + 1\n",
    ") * main_amplitude\n",
    "\n",
    "noise_amplitude = 50\n",
    "noise_signal = lambda noise_frequency: np.sin(2 * noise_frequency * x) * noise_amplitude\n",
    "\n",
    "noise_frequencies = range(1, 25, 2)\n",
    "\n",
    "fig, axs = plt.subplots(\n",
    "    2, math.ceil(len(noise_frequencies) / 2), figsize=(12, 10), sharex=True, sharey=True\n",
    ")\n",
    "fig.suptitle(\"Input signals\")\n",
    "for ax, noise_frequency in zip(axs.ravel(), noise_frequencies):  # type: ignore\n",
    "    ax.set_title(noise_frequency)\n",
    "    ax.plot(time, main_signal + noise_signal(noise_frequency))\n",
    "    ax.plot(time, main_signal, color=(0, 0, 0, 0.25))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40bc5ab0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate initial steady state\n",
    "s = Simulator(get_model(main_signal[0]))\n",
    "s.initialise(get_y0())\n",
    "_, y_ss = s.simulate_to_steady_state()\n",
    "assert y_ss is not None\n",
    "\n",
    "# Calculate reference flux\n",
    "s = Simulator(get_model(main_signal[0]))\n",
    "s.initialise(y_ss)\n",
    "for t_end, pfd in tqdm(enumerate(main_signal, 1), total=len(main_signal)):\n",
    "    s.update_parameter(\"pfd\", pfd)\n",
    "    s.simulate(t_end)\n",
    "\n",
    "c_ref = s.get_full_results_df()\n",
    "v_ref = s.get_fluxes_df()\n",
    "\n",
    "# Calculate by frequency\n",
    "fluxes_per_amplitude = {}\n",
    "for noise_amplitude in [10, 20, 50, 100]:\n",
    "    for noise_frequency in range(1, 25, 2):\n",
    "        s = Simulator(get_model(main_signal[0]))\n",
    "        s.initialise(y_ss)\n",
    "        for t, pfd in tqdm(\n",
    "            enumerate(main_signal + noise_signal(noise_frequency), 1),\n",
    "            total=len(main_signal),\n",
    "        ):\n",
    "            s.update_parameter(\"pfd\", pfd)\n",
    "            s.simulate(t)\n",
    "        assert (v := s.get_fluxes_df()) is not None\n",
    "        fluxes_per_amplitude.setdefault(noise_amplitude, {})[noise_frequency] = v[\n",
    "            \"vRuBisCO\"\n",
    "        ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03f71db5",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(\n",
    "    2,\n",
    "    math.ceil(len(fluxes_per_amplitude[50]) / 2),\n",
    "    figsize=(12, 10),\n",
    "    sharex=True,\n",
    "    sharey=True,\n",
    ")\n",
    "fig.suptitle(\"rubisco flux\")\n",
    "for ax, (freq, v) in zip(axs.ravel(), fluxes_per_amplitude[50].items()):  # type: ignore\n",
    "    ax.plot(v.index, v)\n",
    "    ax.plot(v_ref.index, v_ref[\"vRuBisCO\"], color=(0, 0, 0, 0.25))  # type: ignore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b6f4d0e",
   "metadata": {},
   "outputs": [],
   "source": [
    "error_per_amplitude = pd.DataFrame(\n",
    "    {\n",
    "        amplitude: {\n",
    "            k: np.sqrt(np.mean(np.square(v - v_ref[\"vRuBisCO\"])))  # type: ignore\n",
    "            for k, v in fluxes.items()\n",
    "        }\n",
    "        for amplitude, fluxes in fluxes_per_amplitude.items()\n",
    "    }\n",
    ")\n",
    "\n",
    "fig, axs = plt.subplots(2, 2, figsize=(8, 5), sharex=True)\n",
    "for ax, (ampl, fluxes) in zip(axs.ravel(), error_per_amplitude.items()):  # type: ignore\n",
    "    fluxes.plot(ax=ax, title=f\"u = {ampl}\")\n",
    "fig.text(-0.05, 0.5, \"RMSE\", rotation=90, verticalalignment=\"center\")\n",
    "fig.text(0.5, 0, \"Noise frequency\", horizontalalignment=\"center\")\n",
    "fig.suptitle(\"Low pass filter\")\n",
    "fig.tight_layout()\n",
    "plt.savefig(IMG_PATH / \"bonus-low-pass-filter.png\", bbox_inches=\"tight\", dpi=200)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b9a91a6",
   "metadata": {},
   "source": [
    "### MCA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9c4db00",
   "metadata": {},
   "outputs": [],
   "source": [
    "def scan_response_coefficients_per_pfd(\n",
    "    y_ss, parameters: list[str]\n",
    ") -> tuple[pd.DataFrame, pd.DataFrame]:\n",
    "    crcs = {}\n",
    "    frcs = {}\n",
    "    m = get_model(100)\n",
    "    for i in np.arange(100, 1600, 100):\n",
    "        m.update_parameter(\"pfd\", i)\n",
    "        crc, frc = mca.get_response_coefficients_df(\n",
    "            m, parameters, m.get_full_concentration_dict(y_ss)\n",
    "        )\n",
    "        crcs[i] = crc\n",
    "        frcs[i] = frc\n",
    "    return pd.concat(crcs), pd.concat(frcs)\n",
    "\n",
    "\n",
    "mca_parameters = {\n",
    "    \"pfd\": \"PPFD\",\n",
    "    \"PSIItot\": \"PS2\",\n",
    "    \"PSItot\": \"PS1\",\n",
    "    \"kCytb6f\": r\"b$_6$f\",\n",
    "    \"kcyc\": \"PGR5\",\n",
    "    \"kMehler\": \"Mehler\",\n",
    "    \"V1_base\": \"RuBisCO\",\n",
    "    \"V6_base\": \"FBPase\",\n",
    "    \"V9_base\": \"SBPase\",\n",
    "    \"kcatMDAR\": \"MDAR\",\n",
    "    \"kcat_DHAR\": \"DHAR\",\n",
    "}\n",
    "\n",
    "crcs, frcs = scan_response_coefficients_per_pfd(y_ss, list(mca_parameters))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da68f658",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_rcs(df, title: str) -> Figure:\n",
    "    span = max(df.min().min(), df.max().max())\n",
    "    fig, ax = plt.subplots(figsize=(20, len(df.columns)))\n",
    "    mca.plot_coefficient_heatmap(\n",
    "        df, title=title, xlabel=\"PPFD\", ax=ax, vmin=-span, vmax=span\n",
    "    )\n",
    "    return fig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c844e429",
   "metadata": {},
   "outputs": [],
   "source": [
    "cpd_selection = [\n",
    "    \"PGA\",\n",
    "    \"DHAP\",\n",
    "    \"FBP\",\n",
    "    \"F6P\",\n",
    "    \"SBP\",\n",
    "    \"S7P\",\n",
    "    \"E4P\",\n",
    "    \"X5P\",\n",
    "    \"R5P\",\n",
    "    \"RU5P\",\n",
    "    \"RUBP\",\n",
    "    \"DHA\",\n",
    "    \"MDA\",\n",
    "    \"GSSG\",\n",
    "    \"H2O2\",\n",
    "]\n",
    "\n",
    "_ = plot_rcs(\n",
    "    crcs.xs(\"pfd\", level=1).loc[:, cpd_selection], \"Concentration response coefficients\"\n",
    ")\n",
    "plt.savefig(IMG_PATH / f\"mca-concentration-cc-by-pfd.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a475fac0",
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn_selection = [\n",
    "    \"vFBPase\",\n",
    "    \"vRuBisCO\",\n",
    "    \"v9\",\n",
    "    \"vGR\",\n",
    "    # \"vDHAR\",\n",
    "    # \"v3ASC\",\n",
    "    # \"vAscorbate\",\n",
    "    # \"vMehler\",\n",
    "]\n",
    "\n",
    "_ = plot_rcs(\n",
    "    frcs.xs(\"pfd\", level=1).loc[:, rxn_selection], \"Flux response coefficients\"\n",
    ")\n",
    "plt.savefig(IMG_PATH / f\"mca-flux-cc-by-pfd.png\", bbox_inches=\"tight\", dpi=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86790c07",
   "metadata": {},
   "outputs": [],
   "source": [
    "cols_to_drop = [\n",
    "    \"E_active\",\n",
    "    \"E_inactive\",\n",
    "    \"Keq_ATPsynthase\",\n",
    "    \"Keq_B6f\",\n",
    "    \"PQ_redoxstate\",\n",
    "    \"Fd_redoxstate\",\n",
    "    \"PC_redoxstate\",\n",
    "    \"NADP_redoxstate\",\n",
    "    \"ATP_norm\",\n",
    "]\n",
    "\n",
    "# vmax = crcs.abs().max().max()\n",
    "for pfd, df in crcs.groupby(level=0):\n",
    "    fig, ax = plt.subplots(figsize=(20, 20))\n",
    "    df = (\n",
    "        df.droplevel(0)\n",
    "        .drop(index=[\"pfd\"], columns=cols_to_drop)\n",
    "        .rename(index=mca_parameters)\n",
    "    )\n",
    "    vmax = df.abs().max().max()\n",
    "    mca.plot_coefficient_heatmap(\n",
    "        df,\n",
    "        f\"Concentration CC at PPFD {pfd}\",\n",
    "        annotate=False,\n",
    "        ax=ax,\n",
    "        vmin=-vmax,\n",
    "        vmax=vmax,\n",
    "    )\n",
    "    plt.savefig(\n",
    "        IMG_PATH / f\"mca-concentration-cc-pfd-{pfd:04}.png\",\n",
    "        bbox_inches=\"tight\",\n",
    "        dpi=200,\n",
    "    )\n",
    "    plt.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c0901b80",
   "metadata": {},
   "outputs": [],
   "source": [
    "cols_to_drop = [\n",
    "    \"vE_activation\",\n",
    "    \"vE_inactivation\",\n",
    "    \"vEX_NADPH\",\n",
    "    \"vEX_ATP\",\n",
    "]\n",
    "\n",
    "# vmax = frcs.abs().max().max()\n",
    "for pfd, df in frcs.groupby(level=0):\n",
    "    fig, ax = plt.subplots(figsize=(20, 20))\n",
    "    df = (\n",
    "        df.droplevel(0)\n",
    "        .drop(index=[\"pfd\"], columns=cols_to_drop)\n",
    "        .rename(index=mca_parameters)\n",
    "    )\n",
    "    vmax = df.abs().max().max()\n",
    "    mca.plot_coefficient_heatmap(\n",
    "        df, f\"Flux CC at PPFD {pfd}\", annotate=False, ax=ax, vmin=-vmax, vmax=vmax\n",
    "    )\n",
    "    plt.savefig(\n",
    "        IMG_PATH / f\"mca-flux-cc-pfd-{pfd:04}.png\", bbox_inches=\"tight\", dpi=200\n",
    "    )\n",
    "    plt.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02715956",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "80653a5e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.6 ('py310')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "512px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "1f9a99632784f2e21776966c846309fbce19451a17cd3601adb06f899e5ffe2b"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
