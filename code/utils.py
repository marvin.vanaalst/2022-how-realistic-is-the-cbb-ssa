from __future__ import annotations

import bisect
import itertools as it
import pickle
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, Optional, Union, cast, overload

import numpy as np
import numpy.typing as npt
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import ticker
from matplotlib.axes import Axes as Axis
from matplotlib.figure import Figure
from matplotlib.patches import Patch
from modelbase.ode import Model, Simulator, _Simulate
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from scipy.stats import gaussian_kde
from tqdm.auto import tqdm

from models.cyclicphotosyn2021.models import get_model as _get_model

Axes = npt.NDArray[Axis]  # type: ignore
Array = npt.NDArray[np.float64]


def get_y0() -> dict[str, float]:
    return {
        "PQ": 8.587201814665777,
        "PC": 3.4569708517819633,
        "Fd": 2.172458547415569,
        "ATP": 1.5761459656745005,
        "NADPH": 0.6478778825547451,
        "H": 0.002477719850007161,
        "LHC": 0.7227826655253288,
        "Psbs": 0.9466676059733197,
        "Vx": 0.917120882478094,
        "PGA": 2.0885727980821973,
        "BPGA": 0.001047885611658916,
        "GAP": 0.012644341638134729,
        "DHAP": 0.27817544453105447,
        "FBP": 0.024973139564638265,
        "F6P": 1.1362632219879942,
        "G6P": 2.6134054096212407,
        "G1P": 0.15157751373404657,
        "SBP": 0.07613071682997377,
        "S7P": 0.5121684507402777,
        "E4P": 0.021052218820353498,
        "X5P": 0.05732664706933997,
        "R5P": 0.09602213068792431,
        "RUBP": 0.5949283709571201,
        "RU5P": 0.038408851738451405,
        "MDA": 5.3405908769201765e-06,
        "H2O2": 3.1637054937949144e-06,
        "DHA": 7.089125027925735e-09,
        "GSSG": 3.439853803175048e-09,
        "TR_ox": 0.7541840220043572,
        "E_inactive": 1.735026829811663,
    }


@dataclass
class SimulationResult:
    concentrations: pd.DataFrame
    fluxes: pd.DataFrame


def get_model(initial_pfd: float) -> Model:
    m = _get_model()
    m.update_parameter("pfd", initial_pfd)  # type: ignore
    return cast(Model, m)


###############################################################################
# General fns
###############################################################################


@overload
def michaelis_menten(s: float, vmax: float, km: float) -> float:
    ...


@overload
def michaelis_menten(s: Array, vmax: float, km: float) -> Array:
    ...


@overload
def michaelis_menten(s: pd.Series, vmax: float, km: float) -> pd.Series:
    ...


def michaelis_menten(
    s: Union[float, Array, pd.Series], vmax: float, km: float
) -> Union[float, Array, pd.Series]:
    return s * vmax / (km + s)


def difference_per_time(
    df: Union[pd.Series, pd.DataFrame]
) -> Union[pd.Series, pd.DataFrame]:
    if isinstance(df, pd.Series):
        return pd.Series(
            data=cast(Array, df.to_numpy()[1:]) - cast(Array, df.to_numpy()[:-1]),  # type: ignore
            index=df.index[1:],
        ).dropna()
    return pd.DataFrame(
        data=cast(Array, df.to_numpy()[1:]) - cast(Array, df.to_numpy()[:-1]),  # type: ignore
        index=df.index[1:],
        columns=df.columns,
    ).dropna()


def count_proximity(df: pd.DataFrame, cutoff: float) -> pd.Series:
    close = (df < cutoff).sum()
    far = (df >= cutoff).sum()
    return close * 100 / (far + close)


def get_relative_difference_carbon_fixation(
    v_ode: pd.DataFrame, ss_fluxes_by_pfd: pd.DataFrame
) -> pd.DataFrame:
    ode_fluxes = cast(Array, v_ode["vRuBisCO"].to_numpy().reshape(-1, 1))
    ss_fluxes = cast(Array, ss_fluxes_by_pfd["vRuBisCO"].to_numpy().reshape(1, -1))
    data = np.abs((ode_fluxes - ss_fluxes)) * 100 / ode_fluxes
    return pd.DataFrame(data, index=v_ode.index, columns=ss_fluxes_by_pfd.index)


def get_relative_error_carbon_fixation(
    v_ode: pd.DataFrame, ss_fluxes_by_pfd: pd.DataFrame
) -> pd.Series:
    ode_fluxes = v_ode["vRuBisCO"].sum()
    ss_fluxes = ss_fluxes_by_pfd["vRuBisCO"] * len(v_ode)
    data = (ode_fluxes - ss_fluxes).abs() * 100 / ode_fluxes
    return data


###############################################################################
# Plot helpers
###############################################################################


def format_seconds_to_minutes(ax: plt.Axes, data: pd.DataFrame, every: int = 5) -> None:
    ax.set_xticks(data.index[::every])
    func = lambda x, pos: "{:02.0f}:{:02.0f}".format(*divmod(x, 60))
    func = lambda x, pos: "{:02.0f}".format(x // 60)
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(func))


###############################################################################
# Part 1
###############################################################################


def get_simulation_steps(experiment: pd.Series, step: int) -> pd.Series:
    if step == 1:
        return pd.Series(
            data=experiment.to_numpy(),
            index=(experiment.index - experiment.index[0]).seconds,  # type: ignore
        )
    else:
        # take mean
        return pd.Series(
            data=experiment.groupby(np.arange(len(experiment)) // step).mean().to_numpy(),  # type: ignore
            index=(experiment.index[::step] - experiment.index[0]).seconds,  # type: ignore
        )


def simulate_ode_with_par(
    s: _Simulate, experiment: pd.Series
) -> Optional[SimulationResult]:
    step = 1
    for t_start, pfd in get_simulation_steps(experiment, step).items():
        t_start = int(cast(float, t_start))
        t_end = t_start + 60
        s.update_parameter("pfd", pfd)
        s.simulate(t_end, time_points=np.arange(t_start, t_end, step, dtype="float64"))

    c = s.get_full_results_df()
    v = s.get_fluxes_df()

    if c is not None and v is not None:
        c = cast(pd.DataFrame, c)
        v = cast(pd.DataFrame, v)
        index = np.arange(c.index[0], c.index[-1] + 1, 1)  # type: ignore
        c = c.reindex(index=index, fill_value=None).interpolate()  # type: ignore
        v = v.reindex(index=index, fill_value=None).interpolate()  # type: ignore
        return SimulationResult(
            concentrations=c,
            fluxes=v,
        )
    return None


def simulate_dss_with_par(
    experiment: pd.Series,
    ss_fluxes_by_pfd: pd.DataFrame,
    step: int = 1,
) -> pd.DataFrame:
    res = {}
    for t_start, pfd in get_simulation_steps(experiment, step).items():
        t_start = int(cast(float, t_start))
        t_end = t_start + 60 * step
        v = ss_fluxes_by_pfd.loc[round(pfd)]
        res.update(dict(zip(range(t_start, t_end), it.repeat(v))))
    return pd.DataFrame(res).T


def simulate_rubisco_with_dss(
    experiment: pd.Series,
    ss_fluxes_by_pfd: pd.DataFrame,
    step: int = 1,
) -> pd.Series:
    res = {}
    for t_start, pfd in get_simulation_steps(experiment, step).items():
        t_start = int(cast(float, t_start))
        t_end = t_start + 60 * step
        v = ss_fluxes_by_pfd.loc[round(pfd), "vRuBisCO"]
        res.update(dict(zip(range(t_start, t_end), it.repeat(v))))
    return pd.Series(res)


def simulate_rubisco_with_polyfit(
    experiment: pd.Series,
    polyfit: Array,
    step: int = 1,
) -> pd.Series:
    res = {}
    for t_start, pfd in get_simulation_steps(experiment, step).items():
        t_start = int(cast(float, t_start))
        t_end = t_start + 60 * step
        v = np.polyval(polyfit, pfd)
        res.update(dict(zip(range(t_start, t_end), it.repeat(v))))
    return pd.Series(res)


def simulate_rubisco_with_mm(
    experiment: pd.Series,
    vmax: float,
    km: float,
    step: int = 1,
) -> pd.Series:
    res = {}
    for t_start, pfd in get_simulation_steps(experiment, step).items():
        t_start = int(cast(float, t_start))
        t_end = t_start + 60 * step
        v = michaelis_menten(pfd, vmax, km)
        res.update(dict(zip(range(t_start, t_end), it.repeat(v))))
    return pd.Series(res)


def get_ss_fluxes_by_pfd(
    experiment: pd.Series, y_ss: Array
) -> tuple[pd.DataFrame, pd.DataFrame]:
    s = Simulator(get_model(cast(float, experiment.iloc[0])))
    fluxes_file = Path("results/simulations-ss/fluxes_by_pfd.pickle")
    concs_file = Path("results/simulations-ss/concs_by_pfd.pickle")
    if (not fluxes_file.exists()) or (not concs_file.exists()):
        s.initialise(y_ss)
        ss_concentrations_by_pfd, ss_fluxes_by_pfd = s.parameter_scan_with_fluxes(
            "pfd",
            np.arange(
                round(data_by_one_minute.min()),  # type: ignore
                round(data_by_one_minute.max()),  # type: ignore
                1,
                dtype="float",
            ),
        )
        ss_concentrations_by_pfd.to_pickle(str(concs_file))
        ss_fluxes_by_pfd.to_pickle(str(fluxes_file))

    else:
        ss_fluxes_by_pfd = pd.read_pickle(fluxes_file)
        ss_concentrations_by_pfd = pd.read_pickle(concs_file)
    return ss_concentrations_by_pfd, ss_fluxes_by_pfd


def simulate_experiment(
    experiment: pd.Series, y_ss: Array
) -> tuple[pd.DataFrame, pd.DataFrame]:
    m = get_model(cast(float, experiment.iloc[0]))
    m.update_parameter("pfd", round(experiment.iloc[0]))
    s = Simulator(m)
    fluxes_file = Path("results/simulations-initial/fluxes.pickle")
    concs_file = Path("results/simulations-initial/concs.pickle")
    if (not fluxes_file.exists()) or (not concs_file.exists()):
        s.initialise(y_ss)
        result = simulate_ode_with_par(s, experiment)
        result = cast(SimulationResult, result)  # i know this steady-state isn't None
        c_ode = result.concentrations[m.get_compounds()]
        v_ode = result.fluxes
        c_ode.index = c_ode.index.astype("int64")
        v_ode.index = v_ode.index.astype("int64")
        v_ode.to_pickle(str(fluxes_file))
        c_ode.to_pickle(str(concs_file))
    else:
        v_ode = pd.read_pickle(fluxes_file)
        c_ode = pd.read_pickle(concs_file)
    return c_ode, v_ode


def run_experiment(
    experiment: pd.Series,
) -> tuple[Array, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    # Simulate initial steady state
    # this is both used as the initial condition for the model,
    # as well as the result of the single steady state model
    m = get_model(cast(float, experiment.iloc[0]))
    m.update_parameter("pfd", round(experiment.iloc[0]))
    s = Simulator(m)
    s.initialise(get_y0())
    _, y_ss = s.simulate_to_steady_state()
    y_ss = cast(np.ndarray, y_ss)
    return (
        y_ss,
        *simulate_experiment(experiment, y_ss),
        *get_ss_fluxes_by_pfd(experiment, y_ss),
    )


###############################################################################
# Year experiments
###############################################################################


@dataclass
class Experiment:
    data: pd.Series
    ode_fluxes: pd.DataFrame


def get_coverage_per_day(data_by_one_minute: pd.Series) -> pd.DataFrame:
    lengths: dict[str, dict[str, float]] = {}
    for m, month in data_by_one_minute.groupby(data_by_one_minute.index.month):  # type: ignore
        for d, day in month.groupby(month.index.day):  # type: ignore
            lengths.setdefault(m, {}).setdefault(d, len(day))  # type: ignore
    return pd.DataFrame(lengths)


def get_day_with_most_coverage(df: pd.DataFrame) -> int:
    # alternatively: select day with most coverage for each?
    return cast(int, df.sum(axis=1).idxmax())


def simulate_day_by_hours(
    month: int,
    model_fn: Callable[..., Model],
    ss_concentrations_by_pfd: pd.DataFrame,
    day_with_most_coverage: int,
    data_by_one_minute: pd.Series,
) -> tuple[int, dict[int, Experiment]]:
    simulation_path = Path("results") / "simulations-split"
    experiments: dict[int, Experiment] = {}
    for hour in tqdm(range(24), desc="hour"):
        date = f"2018-{month:02}-{day_with_most_coverage:02}"
        if (file := simulation_path / f"results-{date}-{hour:02}.pickle").exists():
            with open(file, "rb") as f:
                experiment = pickle.load(f)
                if experiment is not None:
                    experiments[hour] = experiment
        else:
            data = data_by_one_minute.loc[f"{date} {hour}:00":f"{date} {hour}:59"]  # type: ignore
            if len(data) > 0:
                starting_pfd = round(data.iloc[0])
                s = Simulator(model_fn(data_by_one_minute))
                s.initialise(dict(ss_concentrations_by_pfd.loc[starting_pfd]))
                result = simulate_ode_with_par(s, data)
                if result is not None:
                    experiment = Experiment(data, result.fluxes)
                    experiments[hour] = experiment
                else:
                    experiment = None
                with open(file, "wb+") as f:
                    pickle.dump(experiment, f)
    return month, experiments


def simulate_day_at_once(
    month: int,
    model_fn: Callable[[pd.Series], Model],
    ss_concentrations_by_pfd: pd.DataFrame,
    day_with_most_coverage: int,
    data_by_one_minute: pd.Series,
) -> tuple[int, Experiment | None]:
    simulation_path = Path("results/simulations-continuous")
    date = f"2018-{month:02}-{day_with_most_coverage:02}"
    if (file := simulation_path / f"results-{date}.pickle").exists():
        with open(file, "rb") as f:
            experiment = pickle.load(f)
    else:
        data = data_by_one_minute.loc[f"{date} 00:00":f"{date} 23:59"]  # type: ignore
        starting_pfd = round(cast(float, data.iloc[0]))
        s = Simulator(model_fn(data_by_one_minute))
        s.initialise(dict(ss_concentrations_by_pfd.loc[cast(pd.Index, starting_pfd)]))  # type: ignore
        result = simulate_ode_with_par(s, data)
        if result is not None:
            experiment = Experiment(data, result.fluxes)
        else:
            experiment = None
        with open(file, "wb+") as f:
            pickle.dump(experiment, f)
    return month, experiment


def get_mean_proximity(
    day: dict[int, Experiment], cutoff: int, ss_fluxes_by_pfd: pd.DataFrame
) -> pd.Series:
    diffs = {}
    for hour, experiment in day.items():
        if experiment is not None and (9 <= hour <= 20):
            rel_diff_rubisco = get_relative_difference_carbon_fixation(
                experiment.ode_fluxes, ss_fluxes_by_pfd
            )
            diffs[hour] = count_proximity(rel_diff_rubisco, cutoff)
    return pd.DataFrame(diffs).mean(axis=1)


def get_proximitiy_of_mean_pfd(
    experiments_split: dict[int, dict[int, Experiment]],
    proximities: dict[int, dict[int, pd.DataFrame]],
) -> dict[int, pd.DataFrame]:
    mean_proximity = {}
    for month, proximity_per_hour in proximities.items():
        mean_proximity[month] = pd.Series(
            {
                hour: proximities[round(experiments_split[month][hour].data.mean())]
                for hour, proximities in proximity_per_hour.items()
            }
        )
    return mean_proximity


def fit_mm_and_poly(
    ss_fluxes_by_pfd: pd.DataFrame, poly_degree: int
) -> tuple[Array, Array]:
    def residual(pars: tuple[float, float], x: Array, y: Array) -> float:
        return np.sqrt(np.mean(np.square(michaelis_menten(x, *pars) - y)))  # type: ignore

    x = cast(Array, ss_fluxes_by_pfd.index.to_numpy())
    y = ss_fluxes_by_pfd["vRuBisCO"].fillna(method="pad").to_numpy()

    mmfit = minimize(
        residual,
        x0=(5, 500),
        bounds=[(0, None), (0, None)],
        args=(x, y),
        method="L-BFGS-B",
    )
    polyfit = np.polyfit(x, y, poly_degree)
    return mmfit.x, polyfit


def get_month_names() -> dict[int, str]:
    return {
        1: "Jan.",
        2: "Feb.",
        3: "Mar.",
        4: "Apr.",
        5: "May",
        6: "June",
        7: "July",
        8: "Aug.",
        9: "Sep.",
        10: "Oct.",
        11: "Nov.",
        12: "Dec.",
    }


def plot_pfd_of_experiment(
    experiment: pd.Series,
    day: pd.Series,
    date: str,
    figsize: tuple[float, float],
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    day.plot(title=date, ax=ax)
    _ = ax.axvspan(
        experiment.index[0],  # type: ignore
        experiment.index[-1],  # type: ignore
        facecolor="C0",
        alpha=0.125,
    )
    _ = ax.legend(
        ["PPFD", "Experiment"],
        loc="upper left",
        bbox_to_anchor=(1.01, 1),
        borderaxespad=0,
    )
    ax.grid(True, which="both")
    return fig


def plot_dss_error_over_time(
    experiment: pd.Series,
    v_ode: pd.DataFrame,
    v_dss: pd.DataFrame,
    axs: Axes | None = None,
    figsize: tuple[float, float] = (16, 6),
) -> tuple[Figure, list[plt.Axes]]:
    def relative(x: pd.Series, y: pd.Series) -> pd.Series:
        return (x - y) * 100 / x

    if axs is None:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=figsize)  # type: ignore
    else:
        ax1, ax2 = axs
        fig = ax1.get_figure()

    x = experiment.index[0] + pd.to_timedelta(v_ode.index, unit="s")  # type: ignore

    def plot1(ax: plt.Axes) -> None:
        ax.plot(x, v_ode["vRuBisCO"], color="C0", label="ODE")
        ax.plot(x, v_dss["vRuBisCO"], color="C1", label="DSS")
        ax.set_title("rubisco flux over time")
        ax.set(ylabel="rubisco flux / (mM / s)")
        ax.legend(loc="lower right")

        ax2 = ax.twinx()
        ax2.fill_between(experiment.index, experiment.values, color="black", alpha=1 / 16)  # type: ignore

    def plot2(ax: plt.Axes) -> None:
        diff = relative(v_ode["vRuBisCO"], v_dss["vRuBisCO"])
        ax.plot(x, diff.dropna(), label="Difference")
        total_error = relative(v_ode["vRuBisCO"].sum(), v_dss["vRuBisCO"].sum())
        ax.axhline(
            total_error,  # type: ignore
            color="black",
            alpha=0.5,
            linestyle="dashed",
            label=f"Total difference = {total_error:.2f} %",
        )
        ax.set_title("Prediction difference")
        ax.set(
            ylabel="Error / %",
        )
        ax.legend(loc="lower left")
        ax2 = ax.twinx()
        ax2.fill_between(experiment.index, experiment.values, color="black", alpha=1 / 16)  # type: ignore

    plot1(ax1)
    plot2(ax2)

    fig.autofmt_xdate()
    fig.tight_layout()
    return fig, [ax1, ax2]


def plot_absolute_total_error_by_metabolite(
    v_ode: pd.DataFrame,
    v_dss: pd.DataFrame,
    *,
    colors: dict[str, str],
    long_rxn_names: dict[str, str],
    selection: list[str] | None = None,
    ax: Axis | None = None,
    figsize: tuple[float, float] = (16, 6),
    order: list[str] | None = None,
) -> tuple[Figure, list[str]]:
    def relative(x: pd.Series, y: pd.Series) -> pd.Series:
        return (x - y) * 100 / x

    rxn_color = {
        "vPS2": "PETC",
        "vPS1": "PETC",
        "vPTOX": "PETC",
        "vNDH": "PETC",
        "vB6f": "PETC",
        "vCyc": "PETC",
        "vFNR": "PETC",
        "vSt12": "PETC",
        "vSt21": "PETC",
        "vATPsynthase": "PETC",
        "vDeepox": "PETC",
        "vEpox": "PETC",
        "vLhcprotonation": "PETC",
        "vLhcdeprotonation": "PETC",
        "vRuBisCO": "CBB",
        "vPGA_kinase": "CBB",
        "vBPGA_dehydrogenase": "CBB",
        "vTPI": "CBB",
        "vAldolase": "CBB",
        "vFBPase": "CBB",
        "vF6P_Transketolase": "CBB",
        "v8": "CBB",
        "v9": "CBB",
        "v10": "CBB",
        "v11": "CBB",
        "v12": "CBB",
        "v13": "CBB",
        "vFdred": "Mehler",
        "vAscorbate": "Mehler",
        "vMDAreduct": "Mehler",
        "vDHAR": "Mehler",
        "v3ASC": "Mehler",
        "vGR": "Mehler",
        "vMehler": "Mehler",
        "vEX_ATP": "General",
        "vEX_NADPH": "General",
        "vE_activation": "CBB",
        "vE_inactivation": "CBB",
        "vFdTrReductase": "CBB",
        "vLeak": "PETC",
        "vG6P_isomerase": "CBB",  # starch really
        "vPhosphoglucomutase": "CBB",  # starch really
        "vStarch": "CBB",  # starch really
        "vdhap": "CBB",
        "vgap": "CBB",
        "vpga": "CBB",
    }

    if selection is None:
        selection = list(rxn_color.keys())

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig = ax.get_figure()

    df = abs(relative(v_ode.sum(), v_dss.sum())).loc[selection]
    if order is None:
        df = df.sort_values(ascending=True)
        order = list(df.index)
    else:
        df = df.loc[order]
    idx = {v: k for k, v in enumerate(df.index)}["vRuBisCO"]
    val = cast(float, df["vRuBisCO"])
    df.plot(
        ax=ax,
        kind="bar",
        color=[colors[rxn_color[i]] for i in df.index],
    )
    ax.set_xticklabels([long_rxn_names.get(i, i) for i in df.index])

    ax.annotate(
        f"rubisco: {df['vRuBisCO']:.2f} %",
        (idx, val),
        xytext=(idx, val + 20),
        arrowprops={"arrowstyle": "->", "color": "black"},
        **{"va": "center", "ha": "center", "fontsize": 18},
    )
    ax.set_ylabel("Error / %")

    ax.legend(
        handles=[
            Patch(label=i, color=f"C{j}")
            for j, i in enumerate(["General", "PETC", "CBB", "Acclimation"])
        ]
    )

    fig.suptitle("Absolute relative total error")
    fig.tight_layout()

    return fig, order


def plot_mehler_ode_vs_dss(
    v_ode: pd.DataFrame,
    v_dss: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    v_ode["vMehler"].plot(ax=ax)
    v_dss["vMehler"].plot(ax=ax)
    ax.set(
        title="Amount of vMehler signal explained by steady-state",
        xlabel="Time / s",
        ylabel="Flux",
    )
    return fig


def plot_absolute_error_by_metabolite(
    v_ode: pd.DataFrame,
    v_dss: list[pd.DataFrame],
    titles: list[str],
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    def relative(x: pd.Series, y: pd.Series) -> pd.Series:
        return (x - y) * 100 / x

    def subplot(
        ax: plt.Axes, v_ode: pd.DataFrame, v_dss: pd.DataFrame, title: str
    ) -> None:
        df = relative(v_ode.sum(), v_dss.sum()).abs().sort_values(ascending=True)
        idx = {v: k for k, v in enumerate(df.index)}["vRuBisCO"]
        val = cast(float, df["vRuBisCO"])
        df.plot(ax=ax, kind="bar")
        ax.annotate(
            f"RuBisCO: {df['vRuBisCO']:.2f} %",
            (idx, val),
            xytext=(idx + 2, val + 20),
            arrowprops={"arrowstyle": "->"},
            **{"va": "center", "ha": "left"},
        )
        ax.set(
            title=title,
            ylabel="Error / %",
            ylim=(0, 100),
        )

    fig, axs = plt.subplots(len(v_dss), 1, figsize=figsize, squeeze=False)

    for ax, i, title in zip(axs.flatten(), v_dss, titles):  # type: ignore
        subplot(ax, v_ode, i, title)

    fig.suptitle("Absolute error", )
    fig.tight_layout()

    return fig


def plot_rubisco_error_per_step_size(
    v_ode: pd.DataFrame,
    df: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
    ax: Axis | None = None,
    label: str | None = None,
) -> Figure:
    def relative(x: float, y: pd.Series) -> pd.Series:
        return (x - y) * 100 / x

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
        ax.set(
            xlabel="Step size / min",
            ylabel="Error / %",
            title="Carboxylation prediction error by step size",
        )
    else:
        fig = ax.get_figure()
    ax.plot(relative(v_ode["vRuBisCO"].sum(), df.sum()).abs(), label=label)
    return fig


def plot_proximity_of_dss_per_pfd(
    experiment: pd.Series,
    v_ode: pd.DataFrame,
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    rel_diff_rubisco = get_relative_difference_carbon_fixation(v_ode, ss_fluxes_by_pfd)

    fig, ax = plt.subplots(figsize=figsize)
    count_proximity(rel_diff_rubisco, 5).plot(label=r"$\pm$ 5 %", ax=ax)
    count_proximity(rel_diff_rubisco, 10).plot(label=r"$\pm$ 10 %", ax=ax)
    count_proximity(rel_diff_rubisco, 20).plot(label=r"$\pm$ 20 %", ax=ax)
    ax.axvline(experiment.min(), label="min pfd", color="black", alpha=0.25)
    ax.axvline(experiment.mean(), label="mean pfd", color="black", alpha=0.5)
    ax.axvline(experiment.max(), label="max pfd", color="black", alpha=0.25)
    ax.set(title="Carbon fixation", xlabel="PFD", ylabel="Proximity of SS solution")
    ax.legend()
    return fig


def plot_ss_rubisco_flux_per_pfd(
    v_ode: pd.DataFrame,
    ss_fluxes_by_pfd: pd.DataFrame,
    experiment: pd.Series,
    min_line: bool = False,
    max_line: bool = False,
    markers: bool = False,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(
        ss_fluxes_by_pfd["vRuBisCO"],
        label="Predicted RuBisCO flux",
    )
    # series is sorted, so we can bisect to get idx of value closest to mean
    mean_idx = bisect.bisect_left(ss_fluxes_by_pfd["vRuBisCO"], v_ode["vRuBisCO"].mean())  # type: ignore
    if markers:
        ax.plot(
            mean_idx,
            ss_fluxes_by_pfd.loc[mean_idx, "vRuBisCO"],
            marker="o",
            color="C1",
            label="mean flux of ODE model",
        )
        ax.plot(
            experiment.mean(),
            ss_fluxes_by_pfd.loc[round(experiment.mean()), "vRuBisCO"],
            marker="o",
            color="black",
            label="SS flux of mean PFD",
        )
    if min_line:
        ax.axvline(
            experiment.min(),
            color="black",
            alpha=0.25,
            label="min PFD flux",
        )
    if max_line:
        ax.axvline(
            experiment.max(),
            color="black",
            alpha=0.25,
            label="max PFD flux",
        )
    ax.set(
        xlabel="PFD",
        ylabel="Flux / (mM / s)",
        title="Steady-state RuBisCO flux by PFD",
    )
    ax.legend()
    return fig


def plot_relative_std(
    c_ode: pd.DataFrame,
    *,
    colors: dict[str, str],
    selection: list[str] | None = None,
    ax: Axis | None = None,
    figsize: tuple[float, float] = (18, 8),
):
    cpd_groups = {
        "ATP": "General",
        "NADPH": "General",
        "PC": "PETC",
        "PQ": "PETC",
        "Fd": "PETC",
        "Psbs": "PETC",
        "X5P": "CBB",
        "R5P": "CBB",
        "RU5P": "CBB",
        "RUBP": "CBB",
        "DHAP": "CBB",
        "GAP": "CBB",
        "S7P": "CBB",
        "FBP": "CBB",
        "E4P": "CBB",
        "SBP": "CBB",
        "Vx": "Mehler",
        "H2O2": "Mehler",
        "MDA": "Mehler",
        "GSSG": "Mehler",
        # "E_inactive": "General",  # how much did thioredoxin regulation change? ~ 33 %
    }

    if selection is None:
        selection = list(cpd_groups.keys())

    # coefficient of variation / relative standard deviation
    cv = (c_ode.std() / c_ode.mean())[selection].sort_values() * 100

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig = ax.get_figure()

    cv.plot(kind="bar", color=[colors[cpd_groups[i]] for i in cv.index], ax=ax)
    ax.set_title("CV of concentrations")
    ax.set_ylabel(r"$\frac{\sigma}{\mu}$ / %", )

    idx = {v: k for k, v in enumerate(cv.index)}["RUBP"]
    val = cast(float, cv["RUBP"])
    ax.annotate(
        f"RuBP: {cv['RUBP']:.0f} %",
        (idx, val),
        xytext=(idx, val + 100),
        arrowprops={"arrowstyle": "->"},
        **{"va": "center", "ha": "center", "fontsize": 18},
    )
    ax.legend(
        handles=[
            Patch(label=i, color=f"C{j}")
            for j, i in enumerate(["General", "PETC", "CBB", "Acclimation"])
        ]
    )
    return fig


def plot_relative_std_fluxes(
    v_ode: pd.DataFrame,
    *,
    colors: dict[str, str],
    long_rxn_names: dict[str, str],
    figsize: tuple[float, float],
):
    rxn_selection = {
        "vPS2": "PETC",
        "vPS1": "PETC",
        "vPTOX": "PETC",
        "vNDH": "PETC",
        "vB6f": "PETC",
        "vCyc": "PETC",
        "vFNR": "PETC",
        "vSt12": "PETC",
        "vSt21": "PETC",
        "vATPsynthase": "PETC",
        "vDeepox": "PETC",
        "vEpox": "PETC",
        "vLhcprotonation": "PETC",
        "vLhcdeprotonation": "PETC",
        "vRuBisCO": "CBB",
        "vPGA_kinase": "CBB",
        "vBPGA_dehydrogenase": "CBB",
        "vTPI": "CBB",
        "vAldolase": "CBB",
        "vFBPase": "CBB",
        "vF6P_Transketolase": "CBB",
        "v8": "CBB",
        "v9": "CBB",
        "v10": "CBB",
        "v11": "CBB",
        "v12": "CBB",
        "v13": "CBB",
        "vFdred": "Mehler",
        "vAscorbate": "Mehler",
        "vMDAreduct": "Mehler",
        "vDHAR": "Mehler",
        "v3ASC": "Mehler",
        "vGR": "Mehler",
        "vMehler": "Mehler",
    }
    # coefficient of variation / relative standard deviation
    cv = (v_ode.std() / v_ode.mean()).loc[rxn_selection.keys()].sort_values() * 100  # type: ignore

    fig, ax = plt.subplots(figsize=figsize)
    cv.plot(kind="bar", color=[colors[rxn_selection[i]] for i in cv.index], ax=ax)
    ax.set_xticklabels([long_rxn_names.get(i, i) for i in cv.index])
    idx = {v: k for k, v in enumerate(cv.index)}["vRuBisCO"]
    val = cast(float, cv["vRuBisCO"])
    ax.annotate(
        f"rubisco: {cv['vRuBisCO']:.0f} %",
        (idx, val),
        xytext=(idx, val + 100),
        arrowprops={"arrowstyle": "->"},
        **{"va": "center", "ha": "center", "fontsize": 14},
    )
    ax.set(title="Relative standard deviation of fluxes")
    ax.set_ylabel(r"$\frac{\sigma}{\mu}$ / %")
    ax.legend(
        handles=[
            Patch(label=i, color=f"C{j}")
            for j, i in enumerate(["General", "PETC", "CBB", "Acclimation"])
        ]
    )
    return fig


def plot_error_of_mean_pfd_by_month_and_hour(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    def relative(x: float, y: float) -> float:
        return (x - y) * 100 / x

    def error_of_mean_pfd(
        experiments_split: dict[int, dict[int, Experiment]],
        ss_fluxes_by_pfd: pd.DataFrame,
    ) -> pd.DataFrame:
        errors: dict[int, dict[int, float]] = {}
        for month, day in experiments_split.items():
            for hour, experiment in day.items():
                total_ode_fixation = experiment.ode_fluxes["vRuBisCO"].sum()
                mean_pfd = round(experiment.data.mean())
                total_ss_fixation = ss_fluxes_by_pfd.loc[mean_pfd, "vRuBisCO"] * len(
                    experiment.ode_fluxes
                )  # type: ignore
                errors.setdefault(month, {})[hour] = abs(
                    relative(total_ode_fixation, total_ss_fixation)  # type: ignore
                )
        return pd.DataFrame(errors)  # type: ignore

    months = get_month_names()

    fig, ax = plt.subplots(figsize=figsize)
    errors = error_of_mean_pfd(experiments_split, ss_fluxes_by_pfd)
    ax.plot(errors)
    ax.set(title="Error of mean PFD", xlabel="Hour", ylabel="Error / %")
    ax.legend(
        [months[i] for i in errors.columns],
        # title="Month",
        loc="upper left",
        bbox_to_anchor=(1.01, 1),
        borderaxespad=0,
    )
    return fig


def plot_error_of_mean_pfd_per_step_size_per_month_per_hour(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    months = get_month_names()

    def relative(x: float, y: pd.Series) -> pd.Series:
        return (x - y) * 100 / x

    hours = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

    fig, axs = plt.subplots(2, len(hours) // 2, figsize=figsize)
    for month, result in experiments_split.items():
        for ax, hour in zip(axs.flatten(), hours):  # type: ignore
            experiment = result[hour]
            ex = experiment.data
            v_ode = experiment.ode_fluxes
            df = pd.DataFrame(
                {
                    1: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 1)["vRuBisCO"],
                    2: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 2)["vRuBisCO"],
                    5: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 5)["vRuBisCO"],
                    10: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 10)["vRuBisCO"],
                    20: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 20)["vRuBisCO"],
                    60: simulate_dss_with_par(ex, ss_fluxes_by_pfd, 60)["vRuBisCO"],
                }  # type: ignore
            )
            ax.plot(
                relative(v_ode["vRuBisCO"].sum(), df.sum()).abs(), label=months[month]
            )
            ax.set(title=f"{hour:02}:00", ylim=(0, 100))

    for ax in axs[-1, :]:  # type: ignore
        ax.set(xlabel="Step size / min")
    for ax in axs[:, 0]:  # type: ignore
        ax.set(ylabel="Error / %")
    axs[0, -1].legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0)  # type: ignore
    fig.suptitle("Carboxylation prediction error by step size", )
    plt.tight_layout()

    return fig


def plot_error_per_pfd_per_month_per_hour(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    months = get_month_names()
    fig, axs = plt.subplots(2, 6, figsize=figsize, sharex=True, sharey=True)
    for month, d in experiments_split.items():
        ax_it = iter(axs.flatten())  # type: ignore
        for hour, experiment in d.items():
            if experiment is not None and (9 <= hour <= 20):
                ax = next(ax_it)
                rel_error_rubisco = get_relative_error_carbon_fixation(
                    experiment.ode_fluxes, ss_fluxes_by_pfd
                )
                ax.plot(rel_error_rubisco, label=months[month])
                ax.set(title=f"{hour:02}:00")
    axs[0, -1].legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0)  # type: ignore
    fig.suptitle("Error per PFD", )
    fig.text(0, 0.5, "Error / %", va="center", rotation=90, )
    fig.text(0.5, 0, "Steady-state PFD", ha="center", )
    fig.tight_layout()
    return fig


def plot_proximity_per_pfd_per_hour_per_month(
    proximity: dict[int, dict[int, pd.DataFrame]],
    suptitle: str,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    months = get_month_names()

    fig, axs = plt.subplots(2, 6, figsize=figsize, sharex=True, sharey=True)
    for month, proximity_per_hour in proximity.items():
        ax_it = iter(axs.flatten())  # type: ignore
        for hour, prox in proximity_per_hour.items():
            ax = next(ax_it)
            ax.plot(prox, label=months[month])
            ax.set(title=f"{hour:02}:00")
    axs[0, -1].legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0)  # type: ignore

    fig.suptitle(suptitle, )
    fig.text(0, 0.5, "Proximity / %", va="center", rotation=90, )
    fig.text(0.5, 0, "Steady-state PFD", ha="center", )
    fig.tight_layout()
    return fig


def plot_proximity_of_mean_pfd_per_hour_per_month(
    mean_proximity: dict[int, pd.Series],
    suptitle: str,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    months = get_month_names()

    fig, axs = plt.subplots(2, 6, figsize=figsize, sharex=True, sharey=True)
    for ax, (month, proximity_per_hour) in zip(axs.ravel(), mean_proximity.items()):  # type: ignore
        ax.plot(proximity_per_hour)
        ax.set(title=months[month])

    fig.text(0, 0.5, "Proximity / %", va="center", rotation=90, )
    fig.text(0.5, 0, "Time of day", ha="center", )
    fig.suptitle(suptitle, )
    fig.tight_layout()
    return fig


def plot_absolute_error_of_mean_pfd_per_month_box(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    def get_abs_diff() -> pd.DataFrame:
        rel_diff: dict[int, dict[int, float]] = {}
        for month, experiments_by_hour in experiments_split.items():
            for hour, experiment in experiments_by_hour.items():
                if experiment is not None and (9 <= hour <= 20):
                    mean_pfd = round(experiment.data.mean())
                    ode_flux = experiment.ode_fluxes["vRuBisCO"].sum()
                    ss_flux = ss_fluxes_by_pfd.loc[mean_pfd, "vRuBisCO"] * len(
                        experiment.ode_fluxes
                    )  # type: ignore
                    rel_diff.setdefault(month, {}).setdefault(
                        hour, abs((ode_flux - ss_flux) * 100 / ode_flux)  # type: ignore
                    )
        df = pd.DataFrame(rel_diff)  # type: ignore
        months = get_month_names()
        df.columns = [months[i] for i in df.columns]  # type: ignore
        return df

    fig, ax = plt.subplots(figsize=figsize)
    get_abs_diff().plot(kind="box", ax=ax)
    ax.set(ylabel="Error / %", title="Absolute error of mean pfd per month")
    fig.autofmt_xdate()
    return fig


def plot_absolute_error_of_mean_pfd_per_month_bar(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    def get_abs_diff() -> pd.Series:
        diff: dict[int, float] = {}
        for month, experiments_by_hour in experiments_split.items():
            ode_flux = 0
            ss_flux = 0
            for hour, experiment in experiments_by_hour.items():
                if experiment is not None and (9 <= hour <= 20):
                    mean_pfd = round(experiment.data.mean())
                    ode_flux += experiment.ode_fluxes["vRuBisCO"].sum()
                    ss_flux += ss_fluxes_by_pfd.loc[mean_pfd, "vRuBisCO"] * len(
                        experiment.ode_fluxes
                    )  # type: ignore
            diff[month] = abs((ode_flux - ss_flux) * 100 / ode_flux)  # type: ignore

        df = pd.Series(diff)
        months = get_month_names()
        df.index = [months[i] for i in df.index]  # type: ignore
        return df

    fig, ax = plt.subplots(figsize=figsize)
    df = get_abs_diff()
    df.plot(kind="bar")
    ax.set(ylabel="Error / %", title="Absolute error per month")
    fig.autofmt_xdate()
    return fig


# def fig7(mean_proximity: dict[int, pd.DataFrame]) -> Figure:
#     months = get_month_names()

#     fig, axs = plt.subplots(1, 3, figsize=(20, 8), sharey=True)
#     for ax, (cutoff, df) in zip(axs.flatten(), mean_proximity.items()):
#         ax.plot(df)
#         ax.set(
#             title=f"{cutoff} %",
#             ylim=(0, 100),
#         )
#     axs[-1].legend(
#         [months[i] for i in df.columns],
#         loc="upper left",
#         bbox_to_anchor=(1.05, 1),
#         borderaxespad=0,
#         title="month",
#     )
#     fig.suptitle("Mean proximity per PFD")
#     fig.text(0, 0.5, "Proximity / %", va="center", rotation=90)
#     fig.text(0.5, 0, "Steady-state PFD", ha="center")
#     fig.tight_layout()
#     return fig


# def fig8(mean_proximity: dict[int, pd.DataFrame]) -> Figure:
#     fig, axs = plt.subplots(1, 3, figsize=(20, 8), sharey=True)

#     for cutoff, df in mean_proximity.items():
#         df.max().plot(ax=axs[0], label=f"{cutoff} %")

#     for cutoff, df in mean_proximity.items():
#         df.mean().plot(ax=axs[1], label=f"{cutoff} %")

#     for cutoff, df in mean_proximity.items():
#         df.min().plot(ax=axs[2], label=f"{cutoff} %")

#     axs[0].set_title("Best case")
#     axs[1].set_title("Mean case")
#     axs[2].set_title("Worst case")

#     for ax in axs.flatten():
#         ax.set(
#             xlabel="Month",
#             ylim=(0, 100),
#         )
#     axs[0].set(ylabel="Proximity")
#     axs[-1].legend()
#     fig.suptitle("Proximity per month")
#     return fig


# def _fig_backup_1(v_ode: pd.DataFrame, ss_fluxes_by_pfd: pd.DataFrame) -> Figure:
#     fig, ax = plt.subplots()

#     def plot_3(ax: plt.Axes) -> None:
#         density = gaussian_kde(ss_fluxes_by_pfd["vRuBisCO"].dropna())
#         density.covariance_factor = lambda: 0.25
#         density._compute_covariance()
#         xs = np.linspace(
#             cast(float, ss_fluxes_by_pfd["vRuBisCO"].min()),
#             cast(float, ss_fluxes_by_pfd["vRuBisCO"].max()),
#         )
#         ax.fill_betweenx(xs, 0, density(xs), color="black", alpha=0.25, label="SS density")
#         ax.axhline(
#             ss_fluxes_by_pfd.loc[round(experiment.mean()), "vRuBisCO"],
#             color="black",
#             alpha=1,
#             label="Flux of mean PFD",
#         )
#         ax.axhline(v_ode["vRuBisCO"].mean(), color="C2", alpha=1, label="mean ode flux")
#         # ax2.axhline(v_dss["vRuBisCO"].mean(), color="C3", alpha=1, label="mean ode flux")
#         ax.set(
#             title="Flux distribution",
#             xlabel="Distribution",
#             ylabel="RuBisCO flux",
#         )
#         ax.legend(loc="lower right")

#     plot_3(ax)
#     return fig


# def _fig_backup_2(
#     experiment: pd.Series[float], v_ode: pd.DataFrame, ss_fluxes_by_pfd: pd.DataFrame
# ) -> Figure:
#     error = get_relative_error_carbon_fixation(v_ode, ss_fluxes_by_pfd)
#     print(f"Best case: {error.min():.2f} %")
#     fig, ax = plt.subplots(figsize=(8, 6))
#     ax.plot(error)
#     ax.axvline(experiment.min(), label="min pfd", color="black", alpha=0.25)
#     ax.axvline(experiment.mean(), label="mean pfd", color="black", alpha=0.5)
#     ax.axvline(experiment.max(), label="max pfd", color="black", alpha=0.25)
#     ax.set(title="Carbon fixation", xlabel="PFD", ylabel="Error / %")
#     ax.legend()
#     return fig


def plot_pfd_distribution_and_mean(
    experiment: pd.Series,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    density = gaussian_kde(experiment)
    xs = np.linspace(30, 2500, 100)
    ax.fill_between(xs, density(xs), alpha=0.5)
    ax.axvline(experiment.mean(), color="C1")
    return fig


def plot_error_distribution_by_sample_size(
    experiment: pd.Series,
    v_ode: pd.DataFrame,
    ss_fluxes_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    def relative(x: float | pd.Series, y: float | pd.Series) -> pd.Series:
        return abs(x - y) * 100 / x  # type: ignore

    def mean_flux_of_samples(n_samples: int) -> pd.DataFrame:
        pfd_choices = [round(i) for i in np.random.choice(pfds, n_samples, p=p)]
        return ss_fluxes_by_pfd.loc[pfd_choices, "vRuBisCO"].mean() * len(v_ode)  # type: ignore

    def error_of_samples(n_samples: int) -> Array:
        y_pred = mean_flux_of_samples(n_samples)
        return abs(y_true - y_pred) * 100 / y_true  # type: ignore

    x = np.linspace(0, 10, 100)
    pfds = np.linspace(30, 2500, 100)  # pfds
    density = gaussian_kde(experiment)
    y_true = v_ode["vRuBisCO"].sum()
    d = density(pfds)
    p = d / d.sum()

    y_mean = ss_fluxes_by_pfd.loc[round(experiment.mean()), "vRuBisCO"] * len(v_ode)  # type: ignore
    v_dss = simulate_dss_with_par(experiment, ss_fluxes_by_pfd, 1)

    fig, ax = plt.subplots(figsize=figsize)
    ax.fill_between(
        x,
        gaussian_kde([error_of_samples(10) for _ in range(1000)])(x),
        label="n = 1e1",
        alpha=0.25,
    )
    ax.fill_between(
        x,
        gaussian_kde([error_of_samples(100) for _ in range(1000)])(x),
        label="n = 1e2",
        alpha=0.25,
    )
    ax.fill_between(
        x,
        gaussian_kde([error_of_samples(int(1e3)) for _ in range(1000)])(x),
        label="n = 1e3",
        alpha=0.25,
    )
    ax.fill_between(
        x,
        gaussian_kde([error_of_samples(int(1e4)) for _ in range(1000)])(x),
        label="n = 1e4",
        alpha=0.25,
    )
    ax.axvline(
        relative(y_true, y_mean),  # type: ignore
        ymin=0,
        alpha=0.5,
        color="black",
        label="error of mean PFD",
    )
    ax.axvline(
        relative(y_true, v_dss["vRuBisCO"].sum()),  # type: ignore
        ymin=0,
        alpha=0.25,
        color="black",
        label="error of 1 min step",
    )
    ax.set(xlabel="Error / %", ylabel="KDE", title="Error distribution by sample size")
    ax.legend()
    return fig


def plot_fit_mm_and_poly(
    ss_fluxes_by_pfd: pd.DataFrame,
    mmfit: Array,
    polyfit: Array,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    x = ss_fluxes_by_pfd.index.to_numpy()
    y = ss_fluxes_by_pfd["vRuBisCO"].fillna(method="pad").to_numpy()
    y_mm = michaelis_menten(x, *mmfit)
    y_poly = np.polyval(polyfit, x)

    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(x, y, label="Steady-state RuBisCO flux")
    ax.plot(x, y_mm, alpha=0.5, label=f"Michaelis Menten, RMSE = {np.sqrt(np.mean(np.square(y - y_mm))):.2f}")  # type: ignore
    ax.plot(x, y_poly, alpha=0.5, label=f"Polynomial, RMSE = {np.sqrt(np.mean(np.square(y - y_poly))):.2f}")  # type: ignore
    ax.set_xlabel("PPFD")
    ax.set_ylabel(r"Flux / $\frac{mM}{s}$")
    ax.legend(loc="lower right")
    ax.set_title("RuBisCO approximations")

    return fig


def plot_rubisco_flux_by_rubp(
    experiment: pd.Series, y_ss: Array, figsize: tuple[float, float] = (8, 8)
) -> Figure:
    m = get_model(cast(float, experiment.iloc[0]))
    print(m.parameters["pfd"])
    fcd = m.get_full_concentration_dict(y_ss) | m.get_parameters()

    y_orig = {i: fcd[i] for i in m.rates["vRuBisCO"].args}
    y_scan = y_orig.copy()
    y_scan["RUBP"] = np.linspace(0, 10, 100)

    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(
        y_scan["RUBP"], m.rates["vRuBisCO"].function(*y_scan.values()), label="Flux"
    )
    ax.plot(
        y_orig["RUBP"],
        m.rates["vRuBisCO"].function(*y_orig.values()),
        marker="o",
        color="C1",
        label="Reference concentration",
    )
    ax.set_title("rubisco flux by RuBP")
    ax.set_xlabel("RuBP / mM")
    ax.set_ylabel("Flux / (mM / s)")
    ax.legend()
    return fig


def plot_rubisco_flux_by_rubp_and_pfd(
    experiment: pd.Series,
    ss_concentrations_by_pfd: pd.DataFrame,
    figsize: tuple[float, float] = (8, 8),
) -> Figure:
    m = get_model(cast(float, experiment.iloc[0]))
    fcd = (
        m.get_full_concentration_dict(ss_concentrations_by_pfd.to_numpy())
        | m.get_parameters()
    )
    y_orig = {i: fcd[i] for i in m.rates["vRuBisCO"].args}
    y_scan = y_orig.copy()
    rubp_scan = np.linspace(0, 10, 100).reshape(-1, 1)
    y_scan["RUBP"] = rubp_scan

    figsize = (16, 10)
    fig, ax = plt.subplots(figsize=figsize, subplot_kw={"projection": "3d"})

    ax.plot_surface(  # type: ignore
        *np.meshgrid(ss_concentrations_by_pfd.index, rubp_scan),
        m.rates["vRuBisCO"].function(*y_scan.values()),
    )  # type: ignore
    ax.plot(
        ss_concentrations_by_pfd.index,
        y_orig["RUBP"],
        m.rates["vRuBisCO"].function(*y_orig.values()),
        label="Reference concentration",
    )
    ax.set_xlabel("\n\nPFD")
    ax.set_ylabel("\n\nRuBP")
    ax.set_zlabel("\n\nFlux")  # type: ignore
    fig.tight_layout()
    ax.legend()
    return fig


def plot_rubisco_approximations_prediction(
    experiment: pd.Series,
    v_ode: pd.DataFrame,
    ss_fluxes_by_pfd: pd.DataFrame,
    polyfit: Any,
    mmfit: Any,
    *,
    figsize: tuple[float, float],
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    x = experiment.index[0] + pd.to_timedelta(v_ode.index, unit="s")  # type: ignore
    y_true = v_ode["vRuBisCO"].sum()

    ax.plot(x, v_ode["vRuBisCO"], label="ODE")
    ax.plot(
        x,
        simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd),
        label=f"DSS, error = {abs((y_true - simulate_rubisco_with_dss(experiment, ss_fluxes_by_pfd).sum()) / y_true):.2%}",
    )
    ax.plot(
        x,
        simulate_rubisco_with_polyfit(experiment, polyfit),
        label=f"Polynomial, error = {abs((y_true - simulate_rubisco_with_polyfit(experiment, polyfit).sum()) / y_true):.2%}",
    )
    ax.plot(
        x,
        simulate_rubisco_with_mm(experiment, *mmfit),
        label=f"Michaelis-Menten, error = {abs((y_true - simulate_rubisco_with_mm(experiment, *mmfit).sum()) / y_true):.2%}",
    )

    # Shade experiment
    ax2 = ax.twinx()
    ax2.fill_between(experiment.index, experiment.values, color="black", alpha=1 / 16)  # type: ignore
    ax.legend()
    ax.set_title("RuBisCO approximations")
    ax.set_ylabel("Flux / (mM/s)")
    return fig


def plot_pfd_distribution_per_hour(
    experiment: pd.Series,
    figsize: tuple[float, float] = (16, 6),
) -> Figure:
    df = pd.DataFrame(
        {
            "hour": experiment.index.hour,  # type: ignore
            "pfd": experiment.to_numpy(),
        }
    )

    fig, axs = plt.subplots(6, 1, figsize=figsize, sharex=True)

    for ax, (hour, gb) in zip(axs.flatten(), df.groupby("hour")):  # type: ignore
        xs = np.linspace(0, 2500, 100)
        density = gaussian_kde(gb["pfd"])
        ax.fill_between(xs, density(xs), alpha=0.5)
        ax.axvline(gb["pfd"].mean(), color="C1")
        ax.set_yticks([])
        ax.set_ylabel(hour, rotation=0, ha="right", va="center", )

    axs.flatten()[-1].set_xlabel("PPFD")  # type: ignore
    fig.suptitle("PPFD distribution per hour", )
    fig.text(0, 0.5, "Hour", rotation=90, )
    fig.tight_layout()
    return fig


def plot_modified_pfd_data(
    experiment: pd.Series, figsize: tuple[float, float]
) -> Figure:
    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(experiment, label="PPFD")
    ax.plot(experiment.clip(upper=900), label="clip(PPFD)", color="C2")
    # ax.plot(michaelis_menten(experiment, *pars_mm), label="michaelis_menten(PFD)")
    # plt.plot(experiment.index, np.polyval(pars_poly, experiment), color="C3")
    ax.legend()
    ax.set_title("Carbon fixation predictions")
    return fig


def plot_predictions_with_corrected_data(
    experiment: pd.Series,
    v_ode: pd.DataFrame,
    ss_fluxes_by_pfd: pd.DataFrame,
    polyfit: Any,
    figsize: tuple[float, float],
) -> Figure:

    y_true = v_ode["vRuBisCO"].sum()

    y_pred_raw = simulate_dss_with_par(experiment, ss_fluxes_by_pfd, 60)["vRuBisCO"]
    y_pred_clip = simulate_dss_with_par(
        experiment.clip(upper=900), ss_fluxes_by_pfd, 60
    )["vRuBisCO"]
    y_pred_clip_poly = simulate_rubisco_with_polyfit(
        experiment.clip(upper=900), polyfit, 60
    )

    x = experiment.index[0] + pd.to_timedelta(v_ode.index, unit="s")  # type: ignore  # type: ignore

    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(x, v_ode["vRuBisCO"], label="ODE")
    ax.plot(
        x,
        y_pred_raw,
        label=f"DSS(PFD), error = {abs(y_true - y_pred_raw.sum()) / y_true:.1%}",
    )
    ax.plot(
        x,
        y_pred_clip,
        label=f"DSS(clip(PFD)), error = {abs(y_true - y_pred_clip.sum()) / y_true:.1%}",
    )
    ax.plot(
        x,
        y_pred_clip_poly,
        label=f"polyfit(clip(PFD)), error = {abs(y_true - y_pred_clip_poly.sum()) / y_true:.1%}",
    )

    ax.legend()
    # Shade experiment
    ax2 = ax.twinx()
    ax2.fill_between(experiment.index, experiment.values, color="black", alpha=1 / 16)  # type: ignore
    ax.set_title("Carbon fixation predictions")
    return fig


def get_predictions_per_month(
    experiments_split: dict[int, dict[int, Experiment]],
    ss_fluxes_by_pfd: pd.DataFrame,
    polyfit: Array,
) -> pd.DataFrame:
    def concat_data(experiments: dict[int, Experiment]) -> tuple[pd.Series, pd.Series]:
        pfd = []
        rubisco = []
        for i in experiments.values():
            pfd.append(i.data)
            rubisco.append(
                pd.Series(
                    data=i.ode_fluxes["vRuBisCO"].values,
                    index=i.data.index[0] + pd.to_timedelta(i.ode_fluxes.index, unit="s"),  # type: ignore
                )
            )
        return pd.concat(pfd), pd.concat(rubisco)  # type: ignore

    predictions = {}
    for month, data in experiments_split.items():
        pfd, rubisco = concat_data(data)
        predictions[month] = {
            "ode": rubisco.sum(),
            "polyfit": simulate_rubisco_with_polyfit(pfd, polyfit, 60).sum(),
            "polyfit clipped @ 800": simulate_rubisco_with_polyfit(
                pfd.clip(upper=800), polyfit, 60
            ).sum(),
            "polyfit clipped @ 900": simulate_rubisco_with_polyfit(
                pfd.clip(upper=900), polyfit, 60
            ).sum(),
            "polyfit clipped @ 1000": simulate_rubisco_with_polyfit(
                pfd.clip(upper=1000), polyfit, 60
            ).sum(),
            "polyfit clipped @ 1100": simulate_rubisco_with_polyfit(
                pfd.clip(upper=1100), polyfit, 60
            ).sum(),
        }
    return pd.DataFrame(predictions).T


def plot_errors_per_month(
    predictions: pd.DataFrame,
    selection: list[str] | None = None,
    figsize: tuple[float, float] = (16, 6),
    legend_kwargs: dict[str, Any] | None = None,
    plot_names: dict[str, str] | None = None,
) -> Figure:
    month_names = get_month_names()
    if plot_names is None:
        plot_names = {}

    # Absolute yearly error
    total = predictions.sum()
    abs_yearly_error = ((total - total["ode"]) / total["ode"]).drop("ode")  # type: ignore

    # Relative error per month
    rel_errors_by_month = (
        (predictions.T - predictions.ode) * 100 / predictions.ode
    ).T.drop("ode", axis=1)
    if selection is not None:
        rel_errors_by_month = rel_errors_by_month.loc[:, selection]

    fig, ax = plt.subplots(figsize=figsize)
    rel_errors_by_month.plot(kind="bar", ylabel="Error / %", ax=ax)
    ax.set_xticklabels([month_names[i] for i in rel_errors_by_month.index])
    ax.set_title("Relative total error per day (60 min steps)")

    if legend_kwargs is None:
        legend_kwargs = {}

    ax.legend(
        [
            f"{plot_names.get(i, i)}, total = {abs_yearly_error[i]:.1%}"
            for i in rel_errors_by_month.columns
        ],
        **legend_kwargs,
    )
    ax.set_ylim(-15, 15)
    return fig
