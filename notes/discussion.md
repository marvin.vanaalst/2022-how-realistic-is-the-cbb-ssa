**Does your model predict large differences in C3 cycle intermediates in the steady-state?**

- maximal CV is less than 50 %, mean is 27 %
- RUBP has range of ~0.5 mM, some other have larger differences

**The ability of simpler models to capture the behaviour of saturating responses to light can be improved by clipping constraint data before averaging. Can this be generalized to some kind of recommendation?**

- Yes. The clipping here is the most robust version I could think of, but in general the mean over a saturating function is weighted. Any form of removing this weight should work

**MM kinetics show saturating behaviour. Is there any application in this principle in how ODE models are simulated?**

- Reactions that carry a lot of metabolic control should be modelled with the highest fidelity possible (e.g. saturation, thermodynamics & inhibition), while other reactions can essentially be linear (or ignored)
- Introducing saturating behaviour onto the reactions that are essentially linear in vivo (e.g. the parameter value was wrong) can introduce a bias that is probably worse than not including it

**We can expand on this to other saturating responses used to constrain models**

- Not in constrained-based models, because the saturation is non-linear, turning a LP into a QP or worse
