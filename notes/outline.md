# Approximating carbon fixation - how realistic is the CBB cycle steady-state assumption?

## Introduction

- challenge of modelling is what you can leave out, we did that at different scales
- Biological systems are complex, thus we also tend to build complex models
- They however, have a larger error surface and are hard to understand
- thus simple models are desirable
- to simplify we need to understand which parts can be ignored


## Results

### Data & model origin

- boring, but needs to be there

### Simplification 1: removal of dynamic metabolite change

- Total error of carbon fixation is very small
- Rapid acclimation mechanisms are predicted badly
- RUBP only varies by 15 % over time, while GSSG varies around 350 %
- Other CBB intermediates vary more than 50 %

### Simplification 2: removal of entire CBB cycle

- polynomial fits with low error

### Simplification 3: mean over an hour instead of high-resolution data

- initial error is 7 % instead of 0.49 %
- since carbon fixation saturates, mean is weighted
- clipping the data is easiest & robust fix for that weighted mean
- clipped error over entire year is 0.1 % for polynomial

## Discussion & Conclusion

### Model reduction / Steady-state nature of the CBB cycle

#### Despite the rapid light fluctuations, simplified model predicts overall rubisco flux well

- However, dynamic error can be high
- Make sure that we communicate that this is a steady-state simplification only
- Let's flesh out here why this is important to be able to make these simplifications
   - Models can be greatly simplified with little sacrifice
   - Maybe some comparisons to how much computational time this saves?
- Why does a good model do a bad job of consistently representing the biochemistry?

#### At the same time, fluxes of rapid acclimation mechanisms are predicted badly

- This can be explained as RUBP pool is stable, while e.g. GSSG pool varies wildly
- Other CBB cycle intermediates however vary a lot
- thus net CO2 fixation can be accurately simulated, even if the underlying metabolism is not
- This is illustrated by the range of possible kinetics that are allowed by a CBB cycle model that give similar rates of carbon flux (C3 Concentrations with random variation in parameters figure)
- The ability of the CBB cycle to have wildly differently modelled pool sizes, but similar rates of net assimilation means that it can be simplified
- This suggests that the C3 cycle is structured such that RUBP concentration is held constant by buffering the changes

#### Low-degree polynomial

- An extreme simplification is a low-degree polynomial can replace the entire CBB cycle, as prediction only depends on PPFD

#### Concrete suggestions

- This observation highlights that we need to be careful to simply use rates of carbon fixation to validate a model since we know a model can be very wrong and still give similar rates of carboxylation
- Future work can test this with more complex models of carbon assimilation that include photorespiration / dynamic CO2 / dynamic temperature
- No reason to think the basic findings will be sensitive to these additional complexities

### Mean transformations for saturating kinetics

- Start this section with a reference to the specific figures that back up the points that follow
- We could further show that high-resolution PPFD data is not required, as long as saturation is accounted for
- Important as often fewer data is available / only a limited set can be used
   - Different types of data can also be gathered at different intervals, for example irradiance can be captured at time scales of seconds while eddy covariance data requires at least 30 minute integrations.
- Taking the mean over the data increases prediction error by ~14x due to weighted mean
- Robust fix for that is to clip the data above a certain threshold
- Clipped models outperform models working on raw data.
- However, low concentrations would also need a modification (not important here, because ...)
- General recommendation here is to check the dynamic response to an input before simply calculating a mean over it
- This method is useful under any context where there is a saturating response to a fluctuating environmental parameter. For example...
   - Light saturation in canopy models of photosynthesis
   - Plants show a saturating CO2 response, while this is less dynamic than light, there are up to 350 PPM swings in CO2 concentrations diurnally in a canopy due to night respiration and the CO2 drawdown of photosynthesis

### Closing words

- Stress that these results aren't (yet) general, as effects like dynamic CO2 and temperature are not accounted for
- In this work, we make important considerations to how ODE models can represent dynamic photosynthetic processes under natural conditions (Maybe this goes in the intro instead)


## DISCUSS

- While the model structure is important, it is also important to know how best to parameterise a model with forcing inputs measured under stochastic environmental conditions
