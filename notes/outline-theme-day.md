# Story theme day presentation

Part 1
- steady-state model has small prediction error for rubisco, but large for acclimation reactions
  - error in carbon fixation wobbles around 0, this is why error is small
- While SBP / E4P are not that stable, RUBP is
- Since RuBP is stable, carbon fixation is essentially just a saturating function of light (constant CO2 assumed)
- Entire ODE model can be replaced by single polynomial with reasonable error

Part 2
- mean doesn't work well for saturating kinetics
- simply clipping the input works surprisingly well
- alternatively, a kernel-density sampling also works well