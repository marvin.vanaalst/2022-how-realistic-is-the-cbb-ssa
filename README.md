# Readme


[![pipeline status](https://gitlab.com/qtb-hhu/modelbase-software/badges/main/pipeline.svg)](https://gitlab.com/marvin.vanaalst/2022-how-realistic-is-the-cbb-ssa/-/commits/main)
[![download pdf](https://img.shields.io/badge/Main-PDF-green)](https://gitlab.com/marvin.vanaalst/2022-how-realistic-is-the-cbb-ssa/-/jobs/artifacts/main/file/tex/main.pdf?job=build)
[![download pdf](https://img.shields.io/badge/Supplementary-PDF-green)](https://gitlab.com/marvin.vanaalst/2022-how-realistic-is-the-cbb-ssa/-/jobs/artifacts/main/file/tex/supplementary.pdf?job=build)


